# TAUMAIC
## Presentation

This is a Discord bot.

TAUMAIC refer to:<br/>
&nbsp;&nbsp;Time - Admin - Utils - Music - AI Chat

## How to run

Before runing the bot you must create a file "token.json" and write:
```
{
	"token": "Your Discord application token",
	"api": "Your API key, require to use API Client"
}
```

Create also a file "apikeys.json" and write:
```
{
	"apikeys": []
}
```

NodeJS is required to run the bot and install dependencies.

You must also install all dependencies by typing the following command inside de project folder:
```npm install```

Compile the TypeScript files into JavaScript files with:
```npm run build```

And then run the bot with:
```npm start```
