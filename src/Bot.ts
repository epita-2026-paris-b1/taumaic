import {
    Client as DiscordClient,
    IntentsBitField as DiscordIntentsBitField,
    Snowflake,
    Partials as DiscordPartials
} from "discord.js";

//import { createEventFromConfig } from './Events';

import { Logger } from "./Log";

import { CommandsManager } from "./Commands";

import { APIServer } from "./api/Server";

import { readFileSync } from "fs";
import { Lock } from "./Lock";

import { MusicSubscription } from "./music/Subscription";

import * as MusicCmds from "./commands/Music";
import * as MiscellaneousCmds from "./commands/Miscellaneous";

import { musicInit } from './music/Music';



const FILENAME_KEYSTORE = "apikeys.json";
const FILENAME_TOKEN = "token.json";
const FILENAME_CONFIG = "config.json";
const FILENAME_LOG = "log.txt";

const API_SERVER_PORT = 8082;



export class TAUMAIC {
    client: DiscordClient;
    apiserver: APIServer;
    cmdmanager: CommandsManager;
    token: string;

    // List of MusicPlayer subscriptions
    musicsubscriptions : Map<Snowflake, MusicSubscription>;

    // Config variables
    help: string;
    helps: Map<string, string>;

    destroyed : boolean = false;
    log : Logger;
    lock : Lock;


    constructor(debug = false) {
        this.log = new Logger();
        this.log.openLogFile(FILENAME_LOG);
        this.log.showDebug(debug);
        this.log.start();

        this.lock = new Lock("taumaic");
        this.lock.lock();

        if(this.lock.islock()) {
            this.client = new DiscordClient({
                intents: [
                    DiscordIntentsBitField.Flags.Guilds,
                    DiscordIntentsBitField.Flags.GuildMessages,
                    DiscordIntentsBitField.Flags.DirectMessages,
                    DiscordIntentsBitField.Flags.GuildVoiceStates
                ],
                partials: [
                    DiscordPartials.Channel
                ]
            });

            this.client.on("error", (err) => this.log.error(err.message));
            this.client.on("warn",  (msg) => this.log.warning(msg));
            this.client.on("debug", (msg) => this.log.debug(msg));

            this.apiserver = new APIServer(this.log);

            this.cmdmanager = new CommandsManager();

            this.musicsubscriptions = new Map<Snowflake, MusicSubscription>();

            this.client.on("ready", (_) => {
                this.log.success("Bot ready");

                this.apiserver.listen(API_SERVER_PORT);

                // Send application command (slash command) global configuration to Discord
                // /!\ DO THIS AS LESS AS POSSIBLE beaucause Discord limit the count of this request type per day /!\
                /*this.cmdmanager.registerSlashCommands.then((_) => {
                    this.log.success("Successfuly registered application commands");
                }, (err) => {
                    this.log.error("Error when registering application commands:\n" + err);
                });*/
            });

            process.on('SIGINT', () => {
                this.shutdown(true);
            });

            process.on('SIGTERM', () => {
                this.shutdown(true);
            });

            process.on('exit', () => {
                this.shutdown(false);
            });
        }
    }


    public init(){
        if(this.lock.islock()) {
            // Load token.json file
            this.token = readJsonFile(FILENAME_TOKEN)["token"];

            // Load apikeys.json file
            this.apiserver.loadKeyStore(FILENAME_KEYSTORE);

            // Load config.json file
            const config = this.applyDefConfig( readJsonFile(FILENAME_CONFIG) );

            this.help = jsonMultilineToString(config["help"]);

            this.helps = new Map<string, string>();
            for(const key of Object.keys(config["helps"])) {
                this.helps.set(key.toLowerCase(), jsonMultilineToString( config["helps"][key] ));
            }

            this.cmdmanager.setPrefix(config.cmdprefix);

            this.log.showDebug(config.debug);

            musicInit(this.log);

            // Registers commands
            this.registerCommands();

            // Events
            // TODO

            this.cmdmanager.listen(this.client);
        }
    }


    private registerCommands(){
        MusicCmds.init(this);
        MiscellaneousCmds.init(this);
        // Gen help command at last
        MiscellaneousCmds.genhelpcmd(this);
    }


    public shutdown(exit : boolean = false){
        if(!this.destroyed) {
            this.log.info("Shutdown bot");
            this.client.destroy();
            this.apiserver.saveKeyStore(FILENAME_KEYSTORE);
            this.lock.unlock();
            this.log.closeLogFile();
            this.destroyed = true;
        }
        if(exit) process.exit();
    }


    public start(){
        if(this.lock.islock()) {
            this.log.debug("Start login");
            this.client.login(this.token);
        } else {
            this.log.fatal("TAUMAIC is already started");
        }
    }


    private applyDefConfig(config) {
        var defconf = {
            "debug": false,
            "cmdprefix": "!",
            "help": "no help",
            "helps": {}
        };
        return Object.assign(defconf, config);
    }
}



function jsonMultilineToString(jsonobj : string | string[]){
    if(typeof jsonobj === "string"){
        return jsonobj;
    } else if(Array.isArray(jsonobj)){
        return jsonobj.join('\n');
    }
    return "<error when parsing config.json>";
}



function readJsonFile(filename : string) : void {
    const content = readFileSync(filename, "utf8");
    return JSON.parse(content);
}
