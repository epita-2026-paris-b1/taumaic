import {
    CommandInteraction,
    Message,
    TextBasedChannel,
    Guild,
    GuildMember,
    ApplicationCommandType,
    ApplicationCommandData,
    ApplicationCommandOptionType,
    ApplicationCommandOptionData,
    ApplicationCommandOptionChoiceData
} from "discord.js";

// The below "Command" type represent a registered command
interface CommandEntry {
    callback: Function;
    format: CommandFormat;
}

type CommandCallback = (cmd: CommandFormat, options: CommandOptions, replier: CommandReplier) => void;


// Manage all bots commands
export  class CommandsManager {
    public prefix: string;
    public commands: Map<string, CommandEntry>;

    public constructor() {
        this.prefix = "";
        this.commands = new Map<string, CommandEntry>();
    }

    public setPrefix(prefix: string) {
        this.prefix = prefix;
    }

    public addCommand(format: CommandFormat, callback: CommandCallback){
        this.commands.set(format.name.toLowerCase(), {
            callback: callback,
            format: format
        });
    }

    public removeCommand(name: string){
        this.commands.delete(name.toLowerCase());
    }

    public listen(client){
        client.on("messageCreate", (msg) => {
            if(!msg.content.startsWith(this.prefix)) return;

            let strargs = splitCommandLine(msg.content.substr(1));
            let cmd = strargs[0].toLowerCase();

            if(this.commands.has(cmd)){
                let cmdinfo = this.commands.get(cmd);
                let args = parseCommandArgs(strargs.slice(1), cmdinfo.format);
                let replier = new CommandReplier("MESSAGE", msg);
                cmdinfo.callback(cmd, new CommandOptions(args), replier);
            }
        });
        
        client.on("interactionCreate", (interaction) => {
            if(!interaction.isCommand()) return;

            let {cmd, args} = parseCommandInteraction(interaction);
            cmd = cmd.toLowerCase();
        
            if(this.commands.has(cmd)){
                let replier = new CommandReplier("COMMAND", interaction);
                this.commands.get(cmd).callback(cmd, new CommandOptions(args), replier);
            }
        });
    }

    public registerSlashCommands(client){
        let data = [];
        this.commands.forEach((cmd, a, b) => {
            data.push( cmd.format.toApplicationCommand() );
        });
        return client.application.commands.set(data);
    }
}



// Generic class for reply to a command (both reply to message and application command)
export class CommandReplier {
    public handle: CommandInteraction | Message;
    public type: "COMMAND" | "MESSAGE";
    public channel: TextBasedChannel;
    public guild: Guild;
    public member: GuildMember;
    
    private message;

    public constructor(type, handle){
        this.handle = handle;
        this.type = type;
        this.channel = handle.channel;
        this.guild = handle.channel;
        this.member = handle.member || handle.author;
        this.message = null;
    }

    public async reply(rep){
        let ret = await this.handle.reply(rep);
        if(this.message !== null) {
            let tmp = this.message;
            this.message = ret;
            this.editReply(tmp);
        } else {
            this.message = ret;
        }
    }

    public async editReply(rep) {
        if(this.message !== null) {
            if(this.handle instanceof CommandInteraction) {
                this.handle.editReply(rep);
            } else {
                this.message.edit(rep);
            }
        } else {
            this.message = rep;
        }
    }
}



// Store and option value
export class CommandOption {
    public name: string;
    public type: ApplicationCommandOptionType;
    public value: any;
    public partial: boolean;

    public constructor(name : string, type : ApplicationCommandOptionType, value, partial = false){
        this.name = name;
        this.type = type;
        this.value = value;
        // A partial option value is for example a guild id but not resolved to a guild
        this.partial = partial;
    }

    // Resolve partial
    public async resolve(guild){
        if(!this.partial) return;

        switch(this.type){
            case ApplicationCommandOptionType.Role:
                if( (this.value = await _resolveMention(guild, this.value)) === null ){
                    this.value = await _resolveRole(guild, this.value);
                }
                break;

            case ApplicationCommandOptionType.User:
                if( (this.value = await _resolveMention(guild, this.value)) === null ){
                    this.value = await _resolveUser(guild, this.value);
                }
                break;

            case ApplicationCommandOptionType.Channel:
                if( (this.value = await _resolveMention(guild, this.value)) === null ){
                    this.value = await _resolveChannel(guild, this.value);
                }
                break;

            case ApplicationCommandOptionType.Mentionable:
                this.value = await _resolveMention(guild, this.value);
                break;
            
            default:
                this.value = null;
        }

        this.partial = false;
    }
}


// Partial command option solving

async function _resolveMention(guild, mention){
    if( mention.endsWith(">") ){
        if( mention.startsWith("<@") ){
            if( mention.charAt(2) === '&' ){
                // It's a role mention
                return await _resolveRole(guild, mention.substring(3, -1));
            }
            else {
                // It's a user mention
                let tmp;
                // Remove the '!' char if present
                if( mention.charAt(2) === '!' ) {
                    tmp = mention.substring(3, -1);
                } else {
                    tmp = mention.substring(2, -1);
                }
                return await _resolveUser(guild, tmp);
            }
        }
        else if( mention.startsWith("<#") ){
            // It's a channel mention
            return await _resolveChannel(guild, mention.substring(2, -1));
        }
    }
    return null;
}

async function _resolveRole(guild, uid){
    try {
        return await guild.roles.fetch(uid);
    } catch(e){
        return null;
    }
}

async function _resolveUser(guild, uid){
    try {
        return await guild.members.fetch(uid);
    } catch(e){
        return null;
    }
}

async function _resolveChannel(guild, uid){
    try {
        return await guild.channels.fetch(uid);
    } catch(e){
        return null;
    }
}



//  Used for parsing and Discord slash commands
export class CommandOptionFormat {
    public name: string;
    public description: string;
    public type: ApplicationCommandOptionType;
    public required: boolean;
    public choices: Array<CommandOptionChoice>;

    constructor(name : string, type : ApplicationCommandOptionType, description : string, required = true, choices = []){
        this.name = name; // Option id
        this.description = description;
        this.type = type // Option value type (can have multiple possible types)
        this.required = required; // If this option is required
        this.choices = choices; // Possible value of this options
    }
}



// Represent a possible value for an option (used for parsing and Discord slash commands)
export class CommandOptionChoice {
    public name: string;
    public type: ApplicationCommandOptionType; // Type of this choice
    public value: any; // Value of this choice
    public description: string; // Dexription of this choice
    public options: Array<CommandOptionFormat>; // If this choice is selected then aditionals options can be applied to command
    public ignorecase : boolean;
    public filter: (arg : string) => boolean; // This function check if the option value is this choice

    public constructor(name, type, value, description = "", options = [], ignorecase = true, filter = null) {
        this.name = name;
        this.value = value;
        this.type = type;
        this.description = description;
        this.options = options;
        this.ignorecase = ignorecase;
        this.filter = filter;
    }
}



// Store command name and options format, it's used for parsing and Discord slash commands
export class CommandFormat {
    public name: string;
    public options: CommandOptionFormat[];
    public description: string;
    public unlimited: boolean;

    public constructor(name : string, description : string, options : CommandOptionFormat[] = [], unlimited : boolean = false){
        this.name = name.toLowerCase();
        this.options = options;
        this.description = description;
        // An unlimited command mean there are unlimited argument count
        // so the last options type is infinitly repeated
        this.unlimited = unlimited;
    }

    public toApplicationCommand() : ApplicationCommandData {
        if(this.description === null || this.description === "") {
            throw new Error("Command description can't be empty or null");
        }

        if(this.name === null || this.name === "") {
            throw new Error("Command name can't be empty or null");
        }

        return {
            name: this.name,
            type: ApplicationCommandType.ChatInput,
            description: this.description,
            options: CommandFormat.genAppCmdOptions(this.options, 0).data
        };
    }

    private static genAppCmdOptionChoices(choices : CommandOptionChoice[]) : ApplicationCommandOptionChoiceData[] {
        let data : ApplicationCommandOptionChoiceData[] = [];

        for(let j = 0; j < choices.length; j++) {
            let c = choices[j];

            if(c.options !== null && c.options.length > 0) {
                throw new Error("Choice can't have options except for subcommand (group)");
            }

            data.push({
                name: c.name,
                value: c.value
            });
        }

        return data;
    }

    private static genAppCmdOptions(options : CommandOptionFormat[], subcmdlayer : number)
        : {data: ApplicationCommandOptionData[], hassubcmd : boolean}
    {
        let hasoptional = false;
        let hassubcmd = false;
        let data = [];

        for(let i = 0; i < options.length; i++) {
            let o = options[i];

            if(o.name === null || o.name === "") {
                throw new Error("Option name can't be empty or null");
            }

            if(o.name.toLowerCase() !== o.name) {
                throw new Error("Option name must be lowercased");
            }

            if(o.description === null || o.description === "") {
                throw new Error("Option description can't be empty or null");
            }

            // If option is required, there must not have optional option before
            if(!o.required) hasoptional = true;
            else if(hasoptional) {
                throw new Error("Required options must be placed first");
            }

            if(o.type === ApplicationCommandOptionType.Subcommand || o.type === ApplicationCommandOptionType.SubcommandGroup) {
                if(subcmdlayer >= 2) {
                    throw new Error("Can't have more than two subcommand layers (you can only have subcommand group and subcommand)");
                }

                if(hasoptional) {
                    throw new Error("Subcommands must be placed before optional options");
                }

                hassubcmd = true;

                let hassubsubcmd = null;
                for(let j = 0; j < o.choices.length; j++) {
                    let c = o.choices[j]; // Choice
                    let co = CommandFormat.genAppCmdOptions(c.options, subcmdlayer + 1); // Choice options

                    // Check if for all choices, options contain subcommand or if for all choices, options don't contain subcommand
                    if(hassubsubcmd !== null && co.hassubcmd !== hassubsubcmd) {
                        throw new Error("Each subcommands group must have subcommands");
                    }
                    hassubsubcmd = co.hassubcmd;

                    data.push({
                        type: co.hassubcmd ? ApplicationCommandOptionType.SubcommandGroup : ApplicationCommandOptionType.Subcommand,
                        name: c.value,
                        description: c.description,
                        required: true,
                        options: co.data
                    });
                }
            }
            else {
                data.push({
                    type: o.type,
                    name: o.name,
                    description: o.description,
                    required: o.required,
                    choices: CommandFormat.genAppCmdOptionChoices(o.choices)
                });
            }
        }

        return {data, hassubcmd};
    }
}



// Store all options passed to a command
export class CommandOptions {
    private options: Map<string, CommandOption>;

    public constructor(options){
        this.options = new Map<string, CommandOption>();
        for(let i = 0; i < options.length; i++){
            let option = options[i];
            this.options.set(option.name, option);
        }
    }

    public get(name : string, def : any = null) : any {
        if(this.options.has(name))
            return this.options.get(name).value;
        return def;
    }
}



function parseCommandInteraction(interaction){
    let options = interaction.options.data;
    let args = [];

    let m = options.length;
    for(let i = 0; i < m; i++){
        let opt = options[i];
        let value;

        if(opt.role){
            value = opt.role;
        } else if(opt.channel){
            value = opt.channel;
        } else if(opt.member){
            value = opt.member;
        } else {
            value = opt.value;
        }
        
        args.push(new CommandOption(opt.name, opt.type, value));
    }

    return {
        cmd: interaction.commandName,
        args: args
    };
}

function parseCommandArgs(strargs : string[], cmdfmt : CommandFormat){
    return parseCommandArgsRec([], strargs, cmdfmt.options, 0, cmdfmt.unlimited);
}

function parseCommandArgsRec(options : CommandOption[], strargs : string[], optionsfmt : CommandOptionFormat[], strargs_offset : number, unlimited : boolean){
    // Parse each argument into correct type with cmdformat
    let m = strargs.length;
    for(let i = strargs_offset; i < m && i < optionsfmt.length; i++){
        let argfmt = optionsfmt[i - strargs_offset];
        let argstr = strargs[i];

        switch(argfmt.type){
            case ApplicationCommandOptionType.Integer:
                options.push(new CommandOption(argfmt.name, argfmt.type, Number.parseInt(argstr)));
                break;
            
            case ApplicationCommandOptionType.Number:
                options.push(new CommandOption(argfmt.name, argfmt.type, Number.parseFloat(argstr)));
                break;
            
            case ApplicationCommandOptionType.Boolean:
                options.push(new CommandOption(argfmt.name, argfmt.type, parseBoolean(argstr)));
                break;
            
            case ApplicationCommandOptionType.String:
                let v = "";

                if(argstr[0] === '"'){
                    v = argstr.substring(1);
                    while(true){
                        i++;
                        if(i >= strargs.length){
                            // TO-DO : Add error throw (quote opened but never closed)
                            break;
                        }

                        let x = strargs[i];
                        if(x[-1] === '"'){
                            v += x.substring(0, x.length-1);
                            break;
                        }

                        v += x;
                    }
                }
                else if(unlimited) {
                    v = strargs.slice(i).join(' ');
                }
                else {
                    v = argstr;
                }

                options.push(new CommandOption(argfmt.name, argfmt.type, v));
                break;
            
            case ApplicationCommandOptionType.SubcommandGroup:
            case ApplicationCommandOptionType.Subcommand:
                let choice = argfmt.choices[0];
                parseCommandArgsRec(options, strargs, choice.options, strargs_offset + i + 1, unlimited);
                break;
            
            case ApplicationCommandOptionType.Role:
            case ApplicationCommandOptionType.Mentionable:
            case ApplicationCommandOptionType.User:
            case ApplicationCommandOptionType.Channel:
                options.push(new CommandOption(argfmt.name, argfmt.type, argstr, true));
                break;
        }
    }

    return options;
}

function splitCommandLine(cmdline){
    let escape = false;
    let quote = '\0';
    let arg = "";
    let args = [];

    let m  = cmdline.length;
    for(let i = 0; i < m; i++){
        let c = cmdline.charAt(i);
        if(escape){
            arg += c;
            escape = false;
        }
        else if(quote !== '\0'){
            if(c === quote){
                // End of quote
                quote = '\0';
            } else {
                arg += c;
            }
        }
        else {
            if(c === '\"' || c === '\''){
                // Start of quote
                quote = c;
            } else if(c === '\\'){
                // Escape next character
                escape = true;
            } else if(c === ' ') {
                args.push(arg);
                arg = "";
            } else {
                arg += c;
            }
        }
    }
    args.push(arg);

    return args;
}

function parseBoolean(str) {
    str = str.toLowerCase();
    switch(str){
        case "0":
        case "false":
        case "n":
        case "no":
        case "non":
        case "faux":
        case "faut":
        case "none":
        case "deny":
            return false;

        case "1":
        case "true":
        case "y":
        case "yes":
        case "oui":
        case "vrai":
        case "vrais":
        case "vraie":
        case "allow":
            return true;
        
        default:
            return null;
    }
}
