/*
import {
    Event,
    ChannelSet
} from "../Events";

import {
    Client as DiscordClient,
    Intents as DiscordIntents,
    Snowflake,
    Constants,
    MessagePayload,
    TextBasedChannel,
    AnyChannel
} from "discord.js";

import * as APIRequest from 'discord.js/src/rest/APIRequest';
import * as RESTManager from 'discord.js/src/rest/RESTManager';

import { Logger } from "../Log";


const lowlevelSendMsgAt_range = 200;
const ping_interval = 500;
const avg_ping_weight = 0.2;
const ping_msg_text = "ping";
//const midnight_datetime = "2022-01-01T00:00:00";
const midnight_datetime = "2021-12-31T21:00:00";
const countdown_initial_value = 10; // Start count down 10s before midnight


export class NewYearEvent extends Event {
    private newyear_msg : any;
    private countdown_msg : any;
    private newyear_channels : ChannelSet;
    private countdown_channels : ChannelSet;
    private music_channels : ChannelSet;
    private realtime_channels : ChannelSet;

    private dst_users;
    private dst_channels;
    private client : DiscordClient;
    private log : Logger;

    private newyear_timeout = null;
    private countdown_timeout = null;

    private ping_msg = null;
    private ping_event = null;
    private ping_enable : boolean;

    private next_msg_ts = 0;

    private ping_channel = null;
    private avg_ping = null;

    private avg_clock = 0;
    private avg_clock_count = 0;

    private newyear_et : number;
    private newyear_ts : number;

    private countdown_step : number;
    private countdown_et : number;

    private rest : RESTManager;


    public constructor(config : any, client : DiscordClient) {
        super(config);

        this.client = client;
        this.rest = new RESTManager(client);

        this.newyear_msg = config.newyear.messages;

        // New year timestamp
        this.newyear_ts = Date.parse(midnight_datetime);
        this.log.debug( "Midnight at: " + new Date(this.newyear_ts).toLocaleString() );
        this.log.debug( "Curent time: " + new Date(Date.now()).toLocaleString() );
        this.newyear_et = this.newyear_ts - 4000; // Call "lowlevelSendMsgAtRealtime" 4 seconds before midnight

        // Countdown
        this.countdown_step = countdown_initial_value;
        this.countdown_et = this.newyear_ts - (countdown_initial_value * 1000);
    }

    public async init() : Promise<void> {
        await this.newyear_channels.resolve(this.client);
        await this.countdown_channels.resolve(this.client);
        await this.music_channels.resolve(this.client);
        await this.realtime_channels.resolve(this.client);

        // Start main loop
        setInterval(this.mainloop, 100);

        // Start pinging
        this.startPings();
    }


    mainloop() {
        let now = Date.now();
        if(now >= this.newyear_et && now <= this.newyear_ts && this.newyear_timeout === null){
            // Send most important critical timing msg
            let failed : TextBasedChannel[] = [];

            this.realtime_channels.foreach((channel : AnyChannel) => {
                if(channel.isText()) {
                    this.newyear_timeout = this.lowlevelSendMsgAtRealtime(channel, this.newyear_msg, this.newyear_ts - this.avg_ping, (ret) => {
                        if(ret === null) failed.push(channel);
                    });
                }
            });

            this.startNewYear();

            for(let channel of failed) {
                channel.send(this.newyear_msg);
            }
        }
        if(now >= this.countdown_et && now <= this.newyear_ts && this.countdown_timeout === null){
            this.countdown_timeout = this.countdown_main();
        }
    }

    countdown_main() {
        let remain = this.newyear_ts - Date.now();
        let remain_s = Math.round(remain / 1000)

        if(this.countdown_channels && remain_s >= 1) {
            this.countdown_channels.foreach((channel : AnyChannel) => {
                if(channel.isText())
                    channel.send("Décompte: " + remain_s + " s");
            });
        }
        
        if(remain > this.countdown_step) {
            return setTimeout(this.countdown_main, ((remain - 1) % this.countdown_step) + 1 );
        }
        return null;
    }

    startNewYear() {
        if(this.newyear_channels) {
            this.newyear_channels.foreach((channel : AnyChannel) => {
                if(channel.isText())
                    channel.send(this.newyear_msg);
            });
        }
    }



    // ================================= Basic functions =================================


    // Low level send message function for time critical code
    parseResponse(res) {
        if (res.headers.get('content-type').startsWith('application/json')) return res.json();
        return res.buffer();
    }

    async lowlevelSendMsg(channel : TextBasedChannel, content : MessagePayload) {
        let messagePayload;
        if (content instanceof MessagePayload) {
            messagePayload = content.resolveData();
        } else {
            messagePayload = MessagePayload.create(channel, content).resolveData();
        }
        const { data, files } = await messagePayload.resolveFiles();

        const route = ['', 'channels', channel.id, 'messages'];

        const routeBucket = [];
        for (let i = 0; i < route.length; i++) {
          // Reactions routes and sub-routes all share the same bucket
          if (route[i - 1] === 'reactions') break;
          // Literal ids should only be taken account if they are the Major id (the Channel/Guild id)
          if (/\d{16,19}/g.test(route[i]) && !/channels|guilds/.test(route[i - 1])) routeBucket.push(':id');
          // All other parts of the route should be considered as part of the bucket identifier
          else routeBucket.push(route[i]);
        }

        const path = route.join('/');
        let options = Object.assign(
            {
                versioned: this.rest.versioned,
                route: routeBucket.join('/'),
            },
            {
               data,
               files 
            }
        );

        const apiRequest = new APIRequest(this.rest, "post", path, options);
        let res = await apiRequest.make();

        if (res.ok) {
            let data = await this.parseResponse(res);
            return data;

            /*let retmsg = this.client.actions.MessageCreate.handle(data).message;
            return {
                sendts: start_time,
                recvts: stop_time,
                msg: retmsg,
                retcode: res.status,
                latancy: stop_time-start_time,
                clkoffset: new Date(res.headers.get('date')).getTime() - ((start_time+stop_time)/2.)
            }*/
        /*}

        throw new Error("Invalide server response when send message");
    }

    lowlevelSendMsgAt(channel : TextBasedChannel, content, at, msg_callback) {
        let timeout_at = Math.max( 0, at - Date.now() );
        return setTimeout(() => {
            this.lowlevelSendMsg(channel, content)
            .then( msg_callback )
            .catch( (err) => {this.log.error(err); msg_callback(null);} );
        }, timeout_at);
    }

    // Real time function !
    // This function block every thing for aproximatively lowlevelSendMsgAt_range milliseconds
    lowlevelSendMsgAtRealtime(channel : TextBasedChannel, content, at, msg_callback) {
        let timeout_at = Math.max( 0, (at - lowlevelSendMsgAt_range) - Date.now() );
        return setTimeout(() => {
            this.stopPings();
            while(Date.now() < at);
            this.lowlevelSendMsg(channel, content)
            .then( (ret) => {this.startPings(); msg_callback(ret);} )
            .catch( (err) => {this.startPings(); this.log.error(err); msg_callback(null);} );
        }, timeout_at);
    }



    // Ping system

    ping_process_msg(ret) {
        if(this.ping_msg) this.ping_msg.delete().catch();

        if(ret){
            this.ping_msg = ret.msg;
            let dt = this.ping_msg.createdTimestamp - ret.sendts;
            this.avg_clock = ( (this.avg_clock * this.avg_clock_count) + ret.clkoffset ) / (this.avg_clock_count + 1);
            this.avg_clock_count++;
            this.log.debug(
                "Ping: Total = " + dt + "ms; " +
                "Clock+Lat = " + (ret.latancy + this.avg_clock) + "ms; " +
                "Clock = " + this.avg_clock + "ms; " +
                "Lat = " + ret.latancy + "ms; " +
                "Offset = " + (this.ping_msg.createdTimestamp - this.next_msg_ts) + " ms"
            );
            if(this.avg_ping === null) this.avg_ping = dt;
            else this.avg_ping = this.avg_ping * (1 - avg_ping_weight) + avg_ping_weight * dt;
        } else {
            this.log.debug("Ping: failed");
        }

        this.ping();
    }

    ping() {
        if(this.ping_channel && this.ping_enable){
            this.next_msg_ts = Date.now() + ping_interval;
            this.ping_event = this.lowlevelSendMsgAt(this.ping_channel, ping_msg_text, this.next_msg_ts - this.avg_ping, this.ping_process_msg);
        } else {
            this.ping_event = setTimeout(this.ping, ping_interval);
        }
    }

    startPings() {
        this.log.debug("Start pinging");
        this.ping_enable = true;
        this.ping();
    }

    stopPings() {
        this.log.debug("Stop pinging");
        this.ping_enable = false;
        if(this.ping_event !== null) clearTimeout(this.ping_event);
    }
}
*/