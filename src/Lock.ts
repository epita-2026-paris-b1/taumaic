// This file implemant a lock system to avoid server running twice simultaneously

import * as fs from "fs";
import {tmpdir} from "os";
import {join} from "path";

const lockdir = tmpdir();


export class Lock {
    name : string;
    fd : number;

    constructor(name : string) {
        this.name = name;
        this.fd = -1;
    }

    public lock() : boolean {
        this.fd = lock(this.name);
        return this.fd > 0;
    }

    public unlock() : boolean {
        if(this.fd <= 0) return false;
        unlock(this.name, this.fd);
        return true;
    }

    public islock() : boolean {
        return this.fd > 0;
    }
}


export function lock(name : string) : number {
    let filename = join(lockdir, name + ".lock");

    // Flag 0x10000000 is to aquire exclusive lock (only Windows and MacOS)
    if(!fs.existsSync(filename)){
        return fs.openSync(filename, fs.constants.O_RDWR | fs.constants.O_CREAT | 0x10000000);
    }

    try {
        return fs.openSync(filename, fs.constants.O_RDWR | 0x10000000);
    } catch(err) {
        if (err.code === 'EBUSY'){
            return -1;
        } else {
            throw err;
        }
    }
}


export function unlock(name : string, fd : number) : void {
    let filename = join(lockdir, name + ".lock");
    fs.closeSync(fd);
    fs.rmSync(filename);
}