export class MusicTrack {
    title : string;
    description : string;
    thumbnail : string;
    duration : number;
    uri : string;
    complete : boolean;

    public constructor(title : string, description : string, thumbnail : string, duration : number, uri : string, complete : boolean){
        this.title = title;
        this.description = description;
        this.thumbnail = thumbnail;
        this.duration = duration;
        this.uri = uri;
        this.complete = complete;
    }

    public resolve() : Promise<void> {
        return new Promise((resolve) => resolve());
    }
}
