import {
    AudioPlayer,
    createAudioPlayer,
    AudioPlayerStatus,
    VoiceConnection,
    joinVoiceChannel,
    createAudioResource,
    VoiceConnectionStatus,
    entersState,
    VoiceConnectionDisconnectReason,
    AudioResource
} from "@discordjs/voice";

import { MusicTrack } from './MusicTrack';

import { Readable } from "stream";


type PlayMode =  "loop" | "loopone" | "noloop";

const PlayModes : {
    LOOP: PlayMode,
    LOOPONE: PlayMode,
    NOLOOP: PlayMode
} = {
    LOOP: "loop",
    LOOPONE: "loopone",
    NOLOOP: "noloop"
};


const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms))


// This file was inspired by
//  https://github.com/discordjs/voice/blob/main/examples/music-bot/src/music/subscription.ts


// A object for each voice channel connection
export class MusicSubscription {
    private voice_channel;
    private text_channel;
    private stream : Readable;
    private tracks : MusicTrack[];
    private mode : PlayMode;
    private volume : number;
    private player : AudioPlayer;
    private curent : number; // Indice of the curent playing track
    private connection : VoiceConnection;
    private waitready : boolean;


    public constructor(voice_channel, text_channel) {
        this.voice_channel = voice_channel;
        this.text_channel = text_channel;
        this.tracks = [];
        this.mode = PlayModes.NOLOOP;
        this.player = createAudioPlayer();
        this.curent = -1;
        this.waitready = false;

        this.connection = joinVoiceChannel({
            channelId: voice_channel.id,
            guildId: voice_channel.guild.id,
            adapterCreator: voice_channel.guild.voiceAdapterCreator,
        });

        this.connection.on('stateChange', async (_, newState) => {
			if (newState.status === VoiceConnectionStatus.Disconnected) {
				if (newState.reason === VoiceConnectionDisconnectReason.WebSocketClose && newState.closeCode === 4014) {
					/*
						If the WebSocket closed with a 4014 code, this means that we should not manually attempt to reconnect,
						but there is a chance the connection will recover itself if the reason of the disconnect was due to
						switching voice channels. This is also the same code for the bot being kicked from the voice channel,
						so we allow 5 seconds to figure out which scenario it is. If the bot has been kicked, we should destroy
						the voice connection.
					*/
                    console.info("MusicSubscription has lost connection");
					try {
						await entersState(this.connection, VoiceConnectionStatus.Connecting, 5_000);
                        console.info("MusicSubscription has (probably) been moved between channels");
						// Probably moved voice channel
					} catch {
						this.connection.destroy();
						// Probably removed from voice channel
                        console.info("MusicSubscription has been disconnected");
					}
				} else if (this.connection.rejoinAttempts < 5) {
					// The disconnect in this case is recoverable, and we also have <5 repeated attempts so we will reconnect.
					await delay((this.connection.rejoinAttempts + 1) * 5_000);
					this.connection.rejoin();
                    console.info("MusicSubscription attempt to rejoin " + (this.connection.rejoinAttempts + 1) + " / 5");
				} else {
					// The disconnect in this case may be recoverable, but we have no more remaining attempts - destroy.
					this.connection.destroy();
                    console.error("MusicSubscription failed to connect to voice channel");
				}
			} else if (newState.status === VoiceConnectionStatus.Destroyed) {
				// Once destroyed, stop the subscription
				this.stop();
                console.info("MusicSubscription stop because of destroyed connection");
			} else if (
				!this.waitready &&
				(newState.status === VoiceConnectionStatus.Connecting || newState.status === VoiceConnectionStatus.Signalling)
			) {
				/*
					In the Signalling or Connecting states, we set a 10 second time limit for the connection to become ready
					before destroying the voice connection. This stops the voice connection permanently existing in one of these
					states.
				*/
				this.waitready = true;
				try {
					await entersState(this.connection, VoiceConnectionStatus.Ready, 20_000);
                    console.info("MusicSubscription has been successfuly connected");
				} catch {
                    console.error("MusicSubscription failed to connect to voice channel");
					this.leave();
				} finally {
					this.waitready = false;
				}
			}
		});

		// Configure audio player
		this.player.on('stateChange', (oldState, newState) => {
			if (newState.status === AudioPlayerStatus.Idle && oldState.status !== AudioPlayerStatus.Idle) {
				// If the Idle state is entered from a non-Idle state, it means that an audio resource has finished playing.
				// The queue is then processed to start playing the next track, if one is available.
                console.info("MusicSubscription play next track");
				this.next();
                if(this.curent !== -1)
                    this._play();
			}
		});

        this.player.on("error", (err) => {
            console.error(err);
        });

        this.connection.on("error", (err) => {
            console.error(err);
        });

        this.connection.subscribe(this.player);
    }


    // Get voice connection
    public getConnection() : VoiceConnection {
        return this.connection;
    }


    // Get music player
    public getPlayer() : AudioPlayer {
        return this.player;
    }


    // Leave voice channel
    public leave(){
        if(this.connection.state.status !== VoiceConnectionStatus.Destroyed)
            this.connection.destroy();
    }


    // Get curent tracks index
    public getCurent() : number {
        return this.curent;
    }


    // Add track to queue
    public addTrack(track : MusicTrack) {
        this.tracks.push(track);
    }


    // Get curent queue
    public getTracks() : MusicTrack[] {
        return this.tracks;
    }
    

    // Remove track from queue
    public removeTrack(index : number) {
        this.tracks.splice(index);

        if(this.curent >= this.tracks.length){
            if(this.tracks.length !== 0 && this.mode === PlayModes.LOOP){
                this.curent %= this.tracks.length;
            } else {
                this.curent = -1;
            }
        }

        if(this.curent !== -1){
            if(index <= this.curent)
                this._play();
        } else {
            this.stop();
        }
    }


    // Shuffle current tracks list from offset to end
    public shuffle(offset : number) {
        let m = this.tracks.length - offset;

        // If is playing and the curent track is after offset then place curent to offset and shuffle all track after curent
        // (so fix the position of curent to start of shuffle)
        if(this.player.state.status === AudioPlayerStatus.Playing && this.curent >= offset){
            m--;
            offset++;
            let x = this.tracks[offset];
            this.tracks[offset] = this.tracks[this.curent];
            this.tracks[this.curent] = x;
            this.curent = offset;
        }

        // m <= 0 mean that offset is equal or greater than number of tracks
        if(m <= 0) return;

        for(let i = 0; i < 4 * m; i++){
            let a = Math.floor(Math.random() * m) + offset;
            let b = Math.floor(Math.random() * m) + offset;
            let x = this.tracks[a];
            this.tracks[a] = this.tracks[b];
            this.tracks[b] = x;
        }
    }


    // Set volume
    public setVolume(volume : number) : void {
        this.volume = volume;
    }


    // Get volume
    public getVolume() : number {
        return this.volume;
    }


    // Set this.curent to next track
    private next() : void {
        if(this.mode === PlayModes.LOOPONE){
            return;
        }
        this.curent++;
        if(this.curent >= this.tracks.length){
            if(this.mode === PlayModes.LOOP){
                this.curent %= this.tracks.length;
            } else {
                this.curent = -1;
            }
        }
    }


    // Skip current track
    public skip() : void {
        this.next()
        if(this.curent !== -1) {
            this._play();
        } else {
            this.stop();
        }
    }


    // Goto music at specific index
    public goto(index : number) : void {
        if(index < 0) index = this.tracks.length + index;
        if(index >= 0 && index < this.tracks.length){
            if(this.curent !== index){
                this.curent = index;
                this._play();
            }
        } else {
            throw new Error("Invalid track index " + index);
        }
    }


    // Restart curent track from begin
    public restart() : void {
        this._play();
    }


    // Pause curent track play (if any)
    public pause() : void {
        this.player.pause();
    }


    // Resume paused track
    public resume() : void {
        this.player.unpause();
    }


    // Resume, start playing curent track or start from beginin
    public play() : void {
        if(this.player.state.status === AudioPlayerStatus.Paused){
            this.resume();
            return;
        }

        if(this.player.state.status === AudioPlayerStatus.Idle){
            if(this.curent === -1 && this.tracks.length > 0)
                this.curent = 0;
            
            if(this.curent >= 0 && this.curent < this.tracks.length)
                this._play();
        }
    }


    // Stop playing music
    public stop() : void {
        this.player.stop(true);
    }


    // Empty the queue and stop playing music
    public clearTracks() : void {
        this.stop();
        this.tracks = [];
        this.curent = -1;
    }


    // Set play mode (loop all track, loop 1 track, no loop)
    public setMode(mode : PlayMode) {
        this.mode = mode;
    }


    // get play mode
    public getMode() : PlayMode {
        return this.mode;
    }


    private _play() : void {
        //if(this.player.state.status !== AudioPlayerStatus.Buffering)
        getAudioResource(this.tracks[this.curent]).then((rep) => this.player.play(rep));
    }
}


function getAudioResource(track : MusicTrack) : Promise<AudioResource<any>> {
    return new Promise<any>((resolve, reject) => {
        // set inlineVolume to true to enable volume control (but decress performence)
        track.resolve().then(() => {
            if(track.uri === undefined || track.uri === null) {
                reject("Don't have audio uri for " + track.title);
            } else {
                resolve(createAudioResource(track.uri, {inlineVolume: false}));
            }
        }).catch(reject);
    });
}
