import { MusicTrack } from './MusicTrack';

export class MusicResponse {
    playlist : boolean;
    tracks : MusicTrack[];
    track : MusicTrack;

    public constructor(playlist : boolean, tracks : MusicTrack[]) {
        this.playlist = playlist;
        this.tracks = tracks;
        if(tracks.length > 0)
            this.track = tracks[0];
        else if(!playlist) {
            throw new Error("Can't have an empty non playlist response when query for music");
        }
    }
}
