"use strict";

//import * as child_process from 'child_process';
import { URL } from 'url';
import { YtbDLWrapper } from './YtbDlWrapper';
import { Logger } from "../Log";
import { MusicTrack } from './MusicTrack';
import { MusicResponse } from './MusicResponse';
import * as path from "path";


// List of YouTube hostnames
const YTB_HOSTNAMES = new Set([
    "music.youtube.com",
    "www.youtube.com",
    "m.youtube.com",
    "youtube.com",
    "youtu.be"
]);


// Convert a youtube URL or query to raw audio URL
var ytbDLWrapper : YtbDLWrapper = null;


// Query can be an direct uri to audio data, a YouTube video/music/playlist ID, a YouTube HTTP URL, or a search query (search with YouTube)
export async function musicQuery(query : string) : Promise<MusicResponse> {
    try {
        let url = new URL(query);
        // If is an URL
        // Check if it's a YouTube URL
        if( url.protocol === "http:" || url.protocol === "https:" ) {
            if( YTB_HOSTNAMES.has(url.hostname) ) {
                // If it's a YouTube URL
                return await ytbDLWrapper.query(query);
            }
            else {
                // If it's not a YouTube URL
                // then it's a direct http URL to audio data
                let pos = query.lastIndexOf('/');
                let title = query;
                if(pos !== -1)
                    title = query.substring(pos + 1);
                let track = new MusicTrack(title, "", "", 0, query, true);
                return new MusicResponse(true, [track]);
            }
        }
        else if( url.protocol === "local:" ) {
            let filepath = url.host + url.pathname;
            let title = path.posix.basename(filepath);
            filepath = path.posix.normalize(filepath);
            // We need to normalize path first in order to remove the '..' for security resons
            // A senario:
            //   Hacker: Hahaha I will access private file on the bot server
            //   Hacker: I have just to write this command: !play "../../../../../home/admin/private.mp3"
            //   Hacker: Ho interesting this audio file ...
            //   Victim: Ho nooooo, somebody have accessed my private audio from a random Discord server!
            filepath = path.join("./local/", filepath);
            let track = new MusicTrack(title, "", "", 0, filepath, true);
            return new MusicResponse(true, [track]);
        }
        else {
            throw new Error("You can't use other protocol than 'http', 'https' or 'local' for security resons");
        }
    }
    catch(e) {
        // If it's not an URI
        // then it's a search query
        return await ytbDLWrapper.query("ytsearch1:" + query);
    }
}


export function musicInit(log : Logger){
    ytbDLWrapper = new YtbDLWrapper(log);
    ytbDLWrapper.startService();
}


/*
function getAudioDataWithYtbDl(query : string) : Promise<Track> {
    new Promise<Track>((resolve, reject) => {
        let ps = child_process.spawn("youtube-dl", ["--dump-json", "--youtube-skip-dash-manifest", "--no-call-home", "--format", "bestaudio[acodec=opus]/bestaudio", query], {shell: false});

        var outbuf = "";
        var errbuf = "";

        ps.stderr.on("data", (data) => {
            errbuf += data.toString("utf8");
        });

        ps.stdout.on("data", (data) => {
            outbuf += data.toString("utf8");
        });

        ps.on("close", (code) => {
            // Show potential errors
            let errlines = errbuf.split('\n');
            for(let i = 0; i < errlines.length; i++){
                let line = errlines[i];
                if(line.startsWith("ERROR: ")){
                    console.error("youtube-dl error: " + line.substr(7));
                }
            }

            let data = null;
            try {
                data = JSON.parse(outbuf);
            } catch(err){
                reject(err);
            }

            if(data !== null){
                resolve(new Track(
                    data["title"],
                    data["description"],
                    data["thumnail"],
                    data["duration"],
                    data["url"]
                ));
            }
        });

        ps.on("error", (err) => {
            reject(err);
        });
    });
}
*/
