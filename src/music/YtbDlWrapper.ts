import * as child_process from 'child_process';
import { Logger } from '../Log';
import { MusicTrack } from './MusicTrack';
import { MusicResponse } from './MusicResponse';


// Timeout before discarding request in ms
const REQUEST_TIMEOUT = 12_000;


export class YtbDlTrack extends MusicTrack {
    id : string;
    wrapper : YtbDLWrapper;

    public constructor(wrapper : YtbDLWrapper, id : string, title : string, description : string, thumbnail : string, duration : number, uri : string, complete : boolean) {
        super(title, description, thumbnail, duration, uri, complete);
        this.id = id;
        this.wrapper = wrapper;
    }

    public resolve() : Promise<void> {
        return new Promise<void>((resolve, reject) => {
            if(!this.complete) {
                this.wrapper.query(this.id).then((rep) => {
                    if(rep.playlist) {
                        throw new Error("YtbDlWrapper response can't be a playlist when trying to resolve an incomplete track");
                    }
                    this.title = rep.track.title;
                    this.description = rep.track.description;
                    this.duration = rep.track.duration;
                    this.thumbnail = rep.track.thumbnail;
                    this.uri = rep.track.uri;
                    this.complete = true;
                    resolve();
                }).catch(reject);
            }
        });
    }
}



const NEWLINE_CHARCODE = '\n'.charCodeAt(0);
export class YtbDLWrapper {
    proc : child_process.ChildProcess;
    requests_callback : Map<number, (response : any) => void>;
    requests_timeout : Map<number, NodeJS.Timeout>;
    request_id : number;
    log : Logger;
    buff : Buffer[];


    public constructor(log : Logger) {
        this.log = log;
        this.requests_callback = new Map<number, (response : any) => void>();
        this.requests_timeout = new Map<number, NodeJS.Timeout>();
        this.request_id = 0;
        this.proc = null;
        this.buff = [];
    }


    public handleResponse(data : string) {
        let rep = JSON.parse(data);

        if(!('id' in rep)) return;
        let id = parseInt(rep['id']);
        if(id === NaN) return;
        if(!this.requests_callback.has(id)) return;

        clearTimeout(this.requests_timeout.get(id));
        this.requests_callback.get(id)(rep);

        this.requests_timeout.delete(id);
        this.requests_callback.delete(id);
    }
    

    public startService(python_path : string = "python") : void {
        if(this.proc !== null) {
            this.log.error("trying to create new ytbdl_wrapper process but there is already one");
            return;
        }

        this.request_id = 0;

        this.proc = child_process.spawn(python_path, ["YtbDlWrapper.py"], {shell: false});

        this.proc.stderr.on("data", (data : Buffer) => {
            this.log.info("YtbDL Wrapper: " + data.toString("utf8"));
        });

        this.proc.stdout.on("data", (data : Buffer) => {
            let parts : Buffer[] = [];
            let lastpos = 0;

            for(let i = 0, m = data.length; i < m; i++) {
                if (data[i] === NEWLINE_CHARCODE) {
                    parts.push(data.subarray(lastpos, i));
                    lastpos = i + 1;
                }
            }

            if (parts.length > 0) {
                this.buff.push(parts[0]);
                this.handleResponse(Buffer.concat(this.buff).toString('utf-8'));
                this.buff = [];
            }

            for (let i = 1; i < parts.length; i++) {
                this.handleResponse(parts[i].toString('utf-8'));
            }
            
            if (lastpos < data.length) { // If data don't end with a line return
                this.buff.push(data.subarray(lastpos));
            }
        });

        this.proc.on("close", (code : number) => {
            let callbacks = this.requests_callback.values();
            for(let callback of callbacks) {
                callback(null);
            }
            this.requests_timeout.clear();
            this.requests_callback.clear();
            this.log.info("ytbdl_wrapper process closed");
        });

        this.proc.on("error", (err) => {
            this.log.error(err);
        });
    }


    private makeRequest(command : string) : Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if(this.proc === null) {
                reject(new Error("trying to make a request to an unstarted ytbdl_wrapper process"));
                return;
            }

            let id = this.request_id++;
            if(!this.proc.stdin.write(id.toString() + ' ' + command + '\n', (err) => {
                if(err !== undefined) {
                    reject(err);
                    return;
                }
                this.requests_callback.set(id, resolve);
                let timeout = setTimeout(() => {
                    this.requests_callback.delete(id);
                    this.requests_timeout.delete(id);
                    reject(new Error("Request timeout"));
                }, REQUEST_TIMEOUT);
                this.requests_timeout.set(id, timeout);
            })) {
                //reject(new Error("Stdin full, wait for 'drain' event not implemented"));
                // TODO
                this.log.warning("Stdin full, wait for 'drain' event not implemented");
            }
        });
    }


    // Convert a youtube URL or query to raw audio URL
    public query(q : string) : Promise<MusicResponse> {
        return new Promise<MusicResponse>((resolve, reject) => {
            this.makeRequest("query \"" + q + "\"").then((rep) => {
                this.log.debug("Receive from ytbdl_wrapper process: " + JSON.stringify(rep));
                
                if('error' in rep) {
                    if('error_msg' in rep) {
                        reject(new Error(rep['error'] + ' : ' + rep['error_msg']));
                    } else {
                        reject(new Error(rep['error']));
                    }
                    return;
                }

                let data = ('content' in rep) ? rep['content'] : null;

                if(data === null) {
                    reject(new Error("null response"));
                    return;
                }

                // Playlist returned
                if('entries' in data) {
                    let tracks = [];
                    for(let entrie of data['entries']) {
                        tracks.push(new YtbDlTrack(
                            this,
                            entrie["id"],
                            entrie["title"],
                            "", "", 0, "",
                            false
                        ));
                    }
                    resolve(new MusicResponse(true, tracks));
                }
                else {
                    resolve(new MusicResponse(false, [new YtbDlTrack(
                        this,
                        data["id"],
                        data["title"],
                        data["description"],
                        data["thumnail"],
                        data["duration"],
                        data["url"],
                        true
                    )]));
                }
            }).catch(reject);
        });
    }
}
