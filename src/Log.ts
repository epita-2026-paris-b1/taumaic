import * as FS from "fs";
import * as readline from 'readline'



type TermColor = string;

type LogInputCallback = (value : string) => void;

interface TypeColors {
    brackets: TermColor,
    type: TermColor,
    text: TermColor
}



// Logger allow to receive input from console (like commands) and print formated logs

export class Logger {
    logfile: FS.WriteStream;
    endline: string;
    oninput: (value : string) => void;
    insep: Buffer;
    inapp: string;
    inbuff: string;
    incursor: number;
    inenable: boolean;
    inprefix: string;
    intmpenable: boolean;
    intmpprefix: string;
    showdebug: boolean;

    stdin: NodeJS.ReadStream;
    stdout: NodeJS.WriteStream;
    stderr: NodeJS.WriteStream;

    colors: {
        time: { brackets: TermColor; text: TermColor; };
        dbg: TypeColors;
        info: TypeColors;
        succ: TypeColors;
        warn: TypeColors;
        err: TypeColors;
        fatal: TypeColors;
    };


    public constructor(){
        this.logfile = null;
        this.endline = '\r\n';
        this.oninput = null;
        this.insep = Buffer.from('\r', 'utf8'); // Char to delimite each input
        this.inapp = '\n'; // Char to append after each input
        this.inbuff = '';
        this.incursor = 0;
        this.inenable = false;
        this.inprefix = '';
        this.intmpenable = false;
        this.intmpprefix = '';
        this.showdebug = false;

        this.stdin  = process.stdin;
        this.stdout = process.stdout;
        this.stderr = process.stderr;

        this.colors = {
            time: {
                brackets: Colors.fg_lightgrey,
                text: Colors.fg_darkgrey
            },

            dbg: {
                brackets: Colors.fg_lightgrey,
                type: Colors.fg_darkgrey,
                text: Colors.fg_lightgrey
            },
            
            info: {
                brackets: Colors.fg_lightgrey,
                type: Colors.fg_lightblue,
                text: Colors.fg_white
            },

            succ: {
                brackets: Colors.fg_lightgrey,
                type: Colors.fg_darkgreen,
                text: Colors.fg_lime
            },

            warn: {
                brackets: Colors.fg_lightgrey,
                type: Colors.fg_gold,
                text: Colors.fg_gold
            },

            err: {
                brackets: Colors.fg_lightgrey,
                type: Colors.fg_red,
                text: Colors.fg_red
            },
            
            fatal: {
                brackets: Colors.fg_lightgrey,
                type: Colors.fg_darkred,
                text: Colors.fg_darkred
            }
        };

        process.on('exit', () => {
            this.closeLogFile();
        });
    }


    private getTimeStrColor(){
        return this.colors.time.brackets + "[" +
               this.colors.time.text + new Date().toLocaleTimeString() +
               this.colors.time.brackets + "]";
    }
    private getTimeStr(){
        return "[" + new Date().toLocaleTimeString() + "]";
    }
    private formatLogMsg(type : string, msg : string){
        return this.getTimeStr() + " ["+type+"] " + msg;
    }
    private formatLogMsgColor(type : string, msg : string, colors : TypeColors){
        return this.getTimeStrColor() +
               colors.brackets + " [" +
               colors.type + type +
               colors.brackets + "] " +
               colors.text + msg + Colors.reset;
    }

    private write(pipe, msg){
        readline.clearLine(pipe, 1);
        readline.cursorTo(pipe, 0);
        pipe.write(msg);
        this.inputWrite();
    }

    private commonLogMsg(pipe, type : string, value : any, colors : TypeColors){
        let msg = new String(value).toString();
        if(this.logfile && this.logfile.writable === true){
            this.logfile.write( this.formatLogMsg(type, msg) + this.endline );
        }
        this.write( pipe, this.formatLogMsgColor(type, msg, colors) + '\n' );
    }

    private modulo(a, b){
        if(a < 0) return b - (a % b);
        return a % b;
    }


    // Inputs functions

    private inputWrite(){
        readline.clearLine(this.stdout, 1);
        readline.cursorTo(this.stdout, 0);
        if(this.intmpenable)
            this.stdout.write(this.intmpprefix + this.inbuff);
        else
            this.stdout.write(this.inprefix + this.inbuff);
        this.inputUpdateCursor(this.inbuff.length);
    }

    private inputGetWidth(){
        return this.stdout.columns;
    }

    private inputGetHeight(){
        return Math.ceil( (this.inbuff.length + this.inprefix.length) / this.stdout.columns );
    }

    private inputGetCursorPos(offset){
        let z = this.inprefix.length + offset;
        let x = z % this.stdout.columns;
        let y = Math.floor( z / this.stdout.columns );
        return {x,y}
    }

    private inputMoveCursor(oldoffset, newoffset){
        let o = this.inputGetCursorPos(oldoffset);
        let n = this.inputGetCursorPos(newoffset);
        readline.moveCursor(this.stdout, this.modulo(n.x,this.stdout.columns)-o.x, n.y-o.y)
    }

    private inputUpdateCursor(old){
        this.inputMoveCursor(old, this.incursor);
    }

    private inputAdd(a){
        if(this.incursor <  this.inbuff.length){
            let tmp = a + this.inbuff.slice(this.incursor);
            this.inbuff = this.inbuff.slice(0,this.incursor) + tmp;
            this.stdout.write(tmp);
            this.incursor += a.length;
            this.inputUpdateCursor(this.inbuff.length);
        } else {
            this.inbuff += a;
            this.stdout.write(a);
            this.incursor += a.length;
        }
    }

    private inputRem(){
        if(this.incursor > 0){
            let tmp = this.inbuff.slice(this.incursor);
            this.inbuff = this.inbuff.slice(0,this.incursor-1) + tmp;
            this.inputMoveCursor(this.incursor,this.incursor-1);
            this.stdout.write(tmp+' ');
            this.incursor--;
            this.inputUpdateCursor(this.inbuff.length+1);
        }
    }


    public debug(msg){
        if(this.showdebug) {
            this.commonLogMsg(this.stdout, "Debug  ", msg, this.colors.dbg);
        }
    }

    public info(msg){
        this.commonLogMsg(this.stdout, "Info   ", msg, this.colors.info);
    }

    public success(msg){
        this.commonLogMsg(this.stdout, "Success", msg, this.colors.succ);
    }

    public warning(msg){
        this.commonLogMsg(this.stdout, "Warning", msg, this.colors.warn);
    }

    public error(msg){
        this.commonLogMsg(this.stderr, "Error  ", msg, this.colors.err);
    }
    
    public fatal(msg){
        this.commonLogMsg(this.stderr, "Fatal  ", msg, this.colors.fatal);
    }


    public openLogFile(filename){
        this.closeLogFile();
        this.logfile = FS.createWriteStream(filename);
    }

    public closeLogFile(){
        if(this.logfile){
            this.logfile.close();
            this.logfile = null;
        }
    }

    public showDebug(v){
        this.showdebug = v;
    }

    public on(name, func : LogInputCallback){
        if(name === 'input')
            this.oninput = func;
        else return false;
        return true;
    }

    public setInPrefix(prefix){
        readline.clearLine(this.stdout, 1);
        this.inprefix = prefix;
        this.inputWrite();
    }

    public input(ask=this.inprefix){
        readline.clearLine(this.stdout, 1);
        this.intmpprefix = ask;
        this.intmpenable = true;
        this.inputWrite();
    }

    public onInput(func : LogInputCallback){
        this.oninput = func;
    }

    public setInSep(sep){
        this.insep = sep;
    }

    public setInApp(app){
        this.inapp = app;
    }

    public setInEnable(v){
        this.inenable = v;
    }


    public start(){
        function startsWith(buff : Buffer, cmp: Buffer, offset : number) : boolean {
            let end = offset + cmp.length;
            if(end > buff.length || offset >= buff.length) return false;
            return cmp.compare(buff, offset, end) === 0;
        }

        // Keys
        const K_UP    = Buffer.from('\u001b[A', 'utf8');
        const K_DOWN  = Buffer.from('\u001b[B', 'utf8');
        const K_RIGHT = Buffer.from('\u001b[C', 'utf8');
        const K_LEFT  = Buffer.from('\u001b[D', 'utf8');

        const K_CTRLC = Buffer.from('\u0003', 'utf8'); // quit
        const K_CTRLD = Buffer.from('\u0004', 'utf8'); // line return
        const K_CTRLH = Buffer.from('\u0008', 'utf8'); // backspace

        if(this.stdin !== null && this.stdin.isTTY) {
            // Set in raw mode (i.e. don't want to hit "enter" to read stdin)
            this.stdin.setRawMode( true );
            this.stdin.resume();
            this.stdin.setEncoding('utf8');

            this.stdin.on('data', (rawdata) => {
                let data;

                if(typeof rawdata === 'string'){
                    data = Buffer.from(rawdata, 'utf8');
                } else {
                    data = rawdata;
                }

                let i = 0;
                while( i < data.length ){
                    if( startsWith(data, K_CTRLC, i) ) { // ctrl-c ( end of text )
                        this.debug("Receive ^C on stdin");
                        process.emit("SIGINT", "SIGINT");
                        i += K_CTRLC.length;
                    }
                    
                    else if( this.inenable || this.intmpenable ){
                        if( startsWith(data, K_CTRLH, i) ){ // ctrl-h ( backspace )
                            this.inputRem();
                            i += K_CTRLH.length;
                        }

                        else if( startsWith(data, K_CTRLD, i) ){
                            i += K_CTRLD.length;
                        }

                        else if( startsWith(data, K_UP, i) ){
                            i += K_UP.length;
                        }

                        else if( startsWith(data, K_DOWN, i) ){
                            i += K_DOWN.length;
                        }

                        else if( startsWith(data, K_RIGHT, i) ){
                            if(this.incursor < this.inbuff.length){
                                this.inputMoveCursor(this.incursor, this.incursor+1);
                                this.incursor++;
                            }
                            i += K_RIGHT.length;
                        }

                        else if( startsWith(data, K_LEFT, i) ){
                            if(this.incursor > 0){
                                this.inputMoveCursor(this.incursor, this.incursor-1);
                                this.incursor--;
                            }
                            i += K_LEFT.length;
                        }
                        
                        else if( startsWith(data, this.insep, i) ){
                            let tmp = this.inbuff;
                            this.inbuff = '';
                            this.stdout.write(this.inapp);
                            this.incursor = 0;

                            this.intmpenable = false;
                            if(this.inenable) this.inputWrite();

                            i += this.insep.length;

                            if(this.oninput) this.oninput(tmp);
                        }
                        
                        else {
                            let chr = data.toString('utf8').charAt(i);
                            this.inputAdd(chr);
                            i++;
                        }
                    }

                    else i++;
                }
            });
        }
    }
}



// Colors code

export const Colors = {
    fg_darkblack    : '\x1b[30m',
    fg_darkred      : '\x1b[31m',
    fg_darkgreen    : '\x1b[32m',
    fg_darkyellow   : '\x1b[33m',
    fg_darkblue     : '\x1b[34m',
    fg_darkmagenta  : '\x1b[35m',
    fg_darkcyan     : '\x1b[36m',
    fg_darkwhite    : '\x1b[37m',

    fg_black        : '\x1b[30m',
    fg_red          : '\x1b[31m',
    fg_green        : '\x1b[32m',
    fg_gold         : '\x1b[33m',
    fg_blue         : '\x1b[34m',
    fg_purple       : '\x1b[35m',
    fg_cyan         : '\x1b[36m',
    fg_lightgrey    : '\x1b[37m',

    fg_lightblack   : '\x1b[30,1m',
    fg_lightred     : '\x1b[31,1m',
    fg_lightgreen   : '\x1b[32,1m',
    fg_lightyellow  : '\x1b[33,1m',
    fg_lightblue    : '\x1b[34,1m',
    fg_lightmagenta : '\x1b[35,1m',
    fg_lightcyan    : '\x1b[36,1m',
    fg_lightwhite   : '\x1b[37,1m',

    fg_darkgrey     : '\x1b[30,1m',
    fg_pink         : '\x1b[31,1m',
    fg_lime         : '\x1b[32,1m',
    fg_yellow       : '\x1b[33,1m',
    fg_magenta      : '\x1b[35,1m',
    fg_white        : '\x1b[37,1m',
    fg_grey         : '\x1b[30,1m',

    darkblack       : '\x1b[30m',
    darkred         : '\x1b[31m',
    darkgreen       : '\x1b[32m',
    darkyellow      : '\x1b[33m' ,
    darkblue        : '\x1b[34m',
    darkmagenta     : '\x1b[35m',
    darkcyan        : '\x1b[36m',
    darkwhite       : '\x1b[37m',

    black           : '\x1b[30m',
    red             : '\x1b[31m',
    green           : '\x1b[32m',
    gold            : '\x1b[33m',
    blue            : '\x1b[34m',
    purple          : '\x1b[35m',
    cyan            : '\x1b[36m',
    lightgrey       : '\x1b[37m',

    lightblack      : '\x1b[30,1m',
    lightred        : '\x1b[31,1m',
    lightgreen      : '\x1b[32,1m',
    lightyellow     : '\x1b[33,1m',
    lightblue       : '\x1b[34,1m',
    lightmagenta    : '\x1b[35,1m',
    lightcyan       : '\x1b[36,1m',
    lightwhite      : '\x1b[37,1m',

    darkgrey        : '\x1b[30,1m',
    pink            : '\x1b[31,1m',
    lime            : '\x1b[32,1m',
    yellow          : '\x1b[33,1m',
    magenta         : '\x1b[35,1m',
    white           : '\x1b[37,1m',
    grey            : '\x1b[30,1m',

    bg_darkblack    : '\x1b[40m',
    bg_darkred      : '\x1b[41m',
    bg_darkgreen    : '\x1b[42m',
    bg_darkyellow   : '\x1b[43m',
    bg_darkblue     : '\x1b[44m',
    bg_darkmagenta  : '\x1b[45m',
    bg_darkcyan     : '\x1b[46m',
    bg_darkwhite    : '\x1b[47m',

    bg_black        : '\x1b[40m',
    bg_red          : '\x1b[41m',
    bg_green        : '\x1b[42m',
    bg_gold         : '\x1b[43m',
    bg_blue         : '\x1b[44m',
    bg_purple       : '\x1b[45m',
    bg_cyan         : '\x1b[46m',
    bg_lightgrey    : '\x1b[47m',

    bg_lightblack   : '\x1b[40,1m',
    bg_lightred     : '\x1b[41,1m',
    bg_lightgreen   : '\x1b[42,1m',
    bg_lightyellow  : '\x1b[43,1m',
    bg_lightblue    : '\x1b[44,1m',
    bg_lightmagenta : '\x1b[45,1m',
    bg_lightcyan    : '\x1b[46,1m',
    bg_lightwhite   : '\x1b[47,1m',

    bg_darkgrey     : '\x1b[40,1m',
    bg_pink         : '\x1b[41,1m',
    bg_lime         : '\x1b[42,1m',
    bg_yellow       : '\x1b[43,1m',
    bg_magenta      : '\x1b[45,1m',
    bg_white        : '\x1b[47,1m',
    bg_grey         : '\x1b[40,1m',

    reset_all       : '\x1b[0m',
    reset_bright    : '\x1b[21m',
    reset_dim       : '\x1b[22m',
    reset_underline : '\x1b[24m',
    reset_blink     : '\x1b[25m',
    reset_reverse   : '\x1b[27m',
    reset_hidden    : '\x1b[28m',

    reset           : '\x1b[0m',

    bright          : '\x1b[1m',
    dim             : '\x1b[2m',
    underline       : '\x1b[4m',
    blink           : '\x1b[5m',
    reverse         : '\x1b[7m', // Invert the bg and fg colors
    hidden          : '\x1b[8m'
}
