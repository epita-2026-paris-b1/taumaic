/*import { CustomEvent } from './events/CustomEvent';
import { NewYearEvent } from './events/NewYearEvent';

import {
    Client as DiscordClient,
    AnyChannel
} from 'discord.js';


// An event is an action to do following a trigger (eg. New year message at 00h00, Bot birthday, ...)


export type TriggerType = "datetime" | "stop" | "start" | "interval";

export class Trigger {
    type : TriggerType;
}


export type DateTimeTriggerPeriod = "weekly" | "daily" | "monthly" | "onetime" | "yearly";

export class DateTimeTrigger extends Trigger {
    periodicity : DateTimeTriggerPeriod;
    datetime : Date;
}


export type ChannelSetType = "all" | "";

export class ChannelSet {
    type : ChannelSet;

    public async foreach(callback) {
        return;
    }

    public async resolve(client : DiscordClient) {
        return;
    }
}

export class TreeChannelSet extends ChannelSet {
    childs : ChannelSet[];

    public async foreach(callback) {
        for(let child of this.childs) {
            await child.foreach(callback);
        }
    }

    public async resolve(client : DiscordClient) {
        for(let child of this.childs) {
            await child.resolve(client);
        }
    }
}

export class IDsChannelSet extends ChannelSet {
    ids : string[];
    channels : AnyChannel[];

    public async resolve(client : DiscordClient) {
        this.channels = [];
        for(let id of this.ids) {
            this.channels.push(await client.channels.fetch(id))
        }
    }

    public async foreach(callback) {
        for(let channel of this.channels) {
            await callback(channel);
        }
    }
}


export type ActionType = "message" | "stop" | "music" | "command";

export class Action {
    public type : ActionType;
}

export class MessageAction extends Action {
    public message : string | any;
}

export class CommandAction extends Action {
    public command : string;
}

export class MusicAction extends Action {
    public query : string;
}


export type EventType = "custom" | "newyear";

export class Event {
    protected type : EventType;
    protected enabled : boolean;
    protected name : string;

    protected constructor(config) {
        this.type = config['type'];
        this.enabled = config['type'];
        this.name = config['name'];
    }

    public init() : Promise<void> {
        return new Promise((resolve) => resolve());
    }
}


export function createEventFromConfig(config : any, client : DiscordClient) : Event {
    let type : EventType = config['type'];
    let enabled = config['enabled'];
    let name = config['name'];

    // Set default values
    if(type === undefined) type = "custom";
    if(enabled === undefined) enabled = true;
    if(name === undefined) name = "unknow";

    if(typeof name !== 'string') throw new Error("Field 'name' of an event must be a string");
    if(typeof enabled !== 'boolean') throw new Error("Field 'enabled' of an event must be a boolean");
    if(typeof type !== 'string') throw new Error("Field 'type' of an event must be a string");

    if(type === "custom") {
        return new NewYearEvent(config, client);
    }
    else if(type === "newyear") {
        return new CustomEvent(config);
    }
    else {
        throw new Error("Unknow event type: " + type);
    }
}
*/