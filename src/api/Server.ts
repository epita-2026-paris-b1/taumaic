import * as http from 'http';

import {
    createHmac,
    generateKey,
    createSecretKey,
    KeyObject
} from 'crypto';
import {URL} from 'url';

import {
    readFileSync,
    writeFileSync
} from "fs";

import {
    API_BASE_ENDPOINT,
    parseAuthorizationHeader,
    readFullBody,
    strToToken,
    tokenToStr
} from './Api';
import { Logger } from '../Log';



interface APIClientData {
    key: KeyObject,
    name: string,
    data_challenge: string,
    auth_challenge: string
}

interface APIResponse {
    code: number,
    data : string,
    challenge : string
}



// Main server class

export class APIServer {
    clientsdata : Map<string, APIClientData>;
    keystorechanged : boolean;
    apiserver : http.Server;
    log : Logger;

    public constructor(log : Logger) {
        this.clientsdata = new Map<string, APIClientData>();
        this.keystorechanged = false;
        this.apiserver = null;
        this.log = log;
    }

    public loadKeyStore(filename : string) : void {
        this.log.debug("Load key store from " + filename);
        let strdata = readFileSync(filename, {encoding: "utf8"});
        let data = JSON.parse(strdata);
        let jsonkeylist = data["apikeys"];
        for(let i = 0; i < jsonkeylist.length; i++){
            let data = jsonkeylist[i];
            let token = strToToken(data["token"]);
            if(token) {
                this.clientsdata.set(token.uuid, {
                    key: createSecretKey(Buffer.from(token.code, "base64")),
                    name: data["name"],
                    data_challenge: null,
                    auth_challenge: null
                });
            }
        }
        this.keystorechanged = false;
    }

    public saveKeyStore(filename : string, force : boolean = false) : void {
        this.log.debug("Save key store to " + filename);
        if(force || this.keystorechanged){
            let jsonkeylist = [];
            this.clientsdata.forEach((data, uuid, _) => {
                jsonkeylist.push({
                    name: data.name,
                    token: tokenToStr({uuid, code: data.key.export().toString("base64")})
                });
            });
            let strdata = JSON.stringify({"apikeys": jsonkeylist}, null, 2);
            writeFileSync(filename, strdata, {encoding: "utf8"});
        }
    }

    public async newKey(name : string) : Promise<void> {
        this.log.debug("Create new key named " + name);
        let uuid = genUUID();
        let key = await genKey();
        this.clientsdata.set(uuid, {
            name: name,
            key: key,
            data_challenge: null,
            auth_challenge: null
        });
        this.keystorechanged = true;
    }

    public listen(port : number) : void {
        this.log.debug("API server listen on " + port);
        if(this.apiserver === null) {
            var self = this;
            this.apiserver = http.createServer((req, res) => {
                // Check header field "Accept" (must accept "application/json" mime type)
                if( "accept" in req.headers && isMimeTypeAccepted(req.headers["accept"], "application/json") ){
                    // Check API endpoint
                    if( req.url.startsWith(API_BASE_ENDPOINT) ){
                        processRequest(self, req).then(
                            ({code, data, challenge}) => {
                                let headers = {
                                    "Content-Type": "application/json"
                                };
                                if(challenge !== null){
                                    headers["Authorization"] = "Challenge " + challenge;
                                }
                    
                                res.writeHead(code, headers);
                                if(data)
                                    res.write(JSON.stringify(data));
                                res.end();
                            },
                            (err) => {
                                self.log.error("Uncaught error: " + err.toString());
                                res.writeHead(500);
                                res.end();
                            }
                        );
                    }
                    else {
                        res.writeHead(404, {"Content-Type": "application/json"});
                        res.write("{\"error\":\"invalid endpoint\"}");
                        res.end();
                    }
                }
                else {
                    res.writeHead(400, {"Content-Type": "text/plain"});
                    res.write("bad request");
                    res.end();
                }
            });
        }
        this.apiserver.listen(port);
    }
}



// Class functions

function checkChallenge(self : APIServer, uuid : string, code : string, req : http.IncomingMessage, body : string){
    if(uuid === null || code === null) return false;

    // Check uuid
    if(!self.clientsdata.has(uuid)) return false;

    // Retreive client specific data
    let clientdata = self.clientsdata.get(uuid);
    let valid = false;

    // Compute data to add to HMAC for integrity check
    let data = "";
    data += req.method;
    //data += req.headers['host'] + ":80" + req.url;
    if(body !== null) data += body;

    // Check data challenge
    if(clientdata.data_challenge !== null) {
        let hmac = createHmac("sha256", clientdata.key);
        hmac.update(clientdata.data_challenge);
        hmac.update(data);
        let digest = hmac.digest("base64");
        self.log.debug("Check data challenge: challenge="+clientdata.data_challenge + "; digest="+digest);
        valid = code === digest;
    }

    // Check auth challenge
    if(!valid && clientdata.auth_challenge !== null) {
        let hmac = createHmac("sha256", clientdata.key);
        hmac.update(clientdata.auth_challenge);
        hmac.update(data);
        let digest = hmac.digest("base64");
        self.log.debug("Check auth challenge: challenge="+clientdata.auth_challenge + "; digest="+digest);
        valid = code === digest;
        // Reset auth challenge (authentication has only one try)
        clientdata.auth_challenge = null;
    }

    return valid;
}

async function processRequest(self : APIServer, req : http.IncomingMessage) : Promise<APIResponse> {
    let path;
    let data;
    let rawdata = null;

    // Read path
    if(req.url.length > API_BASE_ENDPOINT.length)
        path = req.url.substr(API_BASE_ENDPOINT.length).toLowerCase();
    else
        path = null;

    // Read body data
    try {
        rawdata = (await readFullBody(req));
    } catch(err) {
        self.log.debug("Error when reading request data: " + err.message);
        rawdata = null;
        data = null;
    }

    // Parsing request json data
    if(rawdata !== null){
        try {
            if(rawdata !== "") data = JSON.parse(rawdata);
            else data = {};
        } catch(err) {
            self.log.debug("Error when parsing request data: " + err.message);
            data = null;
        }
    }

    // Read token
    let auth = parseAuthorizationHeader(req.headers["authorization"]);
    
    self.log.debug("Received request: token="+auth.value + "; path="+path + "; data="+rawdata);

    if(path !== null && data !== null && auth !== null && auth.type === "Digest") {
        let {uuid, code} = strToToken(auth.value);

        if(checkChallenge(self, uuid, code, req, rawdata)){
            let clientdata = self.clientsdata.get(uuid);
            let challenge = genRandomStr(24);
            clientdata.data_challenge = challenge;
            self.log.debug("Set next data challenge to " + challenge);
            
            switch(path){
                case "restart":
                    return {code: 501, data: null, challenge}; // 501 : Not implemented
                
                case "status":
                    return {code: 501, data: null, challenge}; // 501 : Not implemented
                
                case "update":
                    return {code: 501, data: null, challenge}; // 501 : Not implemented

                case "newkey":
                    if("name" in data){
                        self.newKey(data["name"]).then(() => {
                            return {code: 200, data: null, challenge}; // 200 : OK
                        }, (err) => {
                            return {code: 500, data: "{\"error\": \""+err.message+"\"}", challenge}; // 500 : Internal Server Error
                        });
                    }
                
                case "auth":
                    return {code: 200, data: null, challenge}; // 200 : OK
                
                default:
                    return {code: 404, data: null, challenge}; // 404 : Not found
            }
        }
        else {
            if(uuid !== null && self.clientsdata.has(uuid)){
                let clientdata = self.clientsdata.get(uuid);
                let challenge = genRandomStr(24);
                clientdata.auth_challenge = challenge;
                self.log.debug("Set next auth challenge to " + challenge);
                return {code: 401, data: "{\"error\": \"bad token\"}", challenge}; // 401 : Unauthorized
            }
        }
    }

    return {code: 400, data: "{\"error\": \"bad request\"}", challenge: null}; // 400 : Bad Request
}



// Utils functions

function isMimeTypeAccepted(accept : string, mime : string) : boolean {
    let [mimea, mimeb] = mime.split('/');

    let types = accept.split(", ");

    for(let i = 0; i < types.length; i++){
        let type = types[i];
        if( type === "*/*" ||
            type === "*" ||
            type === mimea + "/*" ||
            type === mime
        ){
            return true;
        }
    }

    return false;
}



// Key and UUID generation functions

const ALPHANUM_MAP = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'];

function genUUID() : string {
    let r = "";

    let x = Math.abs(Math.imul(Date.now(), 1731628575));
    let y = Math.round(x / (0xFFFFFFFF / Math.pow(ALPHANUM_MAP.length, 6)));

    for(let i = 0; i < 6; i++){
        r += ALPHANUM_MAP[y % ALPHANUM_MAP.length];
        y = Math.round(y / ALPHANUM_MAP.length);
    }

    r += genRandomStr(18);

    return r;
}

function genRandomStr(length : number) : string {
    let r = "";
    for(let i = 0; i < length; i++){
        let z = Math.floor(Math.random() * ALPHANUM_MAP.length);
        r += ALPHANUM_MAP[z];
    }
    return r;
}

function genKey() : Promise<KeyObject> {
    return new Promise((resolve, reject) => {
        generateKey("hmac", {length: 256}, (err, key) => {
            if(err) reject(err);
            else resolve(key);
        });
    });
}
