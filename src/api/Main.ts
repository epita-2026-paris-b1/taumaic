import * as process from "process";
import { readFileSync } from "fs";

import { Logger } from "../Log";
import { Lock } from "../Lock";
import { APIServer } from "./Server";
import {
    APIClient,
    APIClientResponse
} from "./Client";



const DEBUG = true;

const log = new Logger();
const lock = new Lock("taumaic");

let serverapi = null;



function main() {
    if(process.argv.length === 2){
        client();
    }
    else if(process.argv.length === 3){
        if(process.argv[2] === "server"){
            server();
        } else if(process.argv[2] === "client"){
            client();
        } else {
            log.fatal("Invalid command line argument \""+process.argv[2]+"\" (must be \"client\" or \"server\")");
        }
    }
    else {
        log.fatal("Invalid command line argument count");
    }
}



function server() {
    if(lock.lock()) {
        init();

        const api = new APIServer(log);
        serverapi = api;
        
        api.loadKeyStore("apikeys.json");

        log.onInput((msg) => {
            let argv = msg.split(' ');
            if(argv.length > 0){
                let cmd = argv[0].toLowerCase();
                
                switch(cmd) {
                    case "newkey":
                        if(argv.length === 2){
                            api.newKey(argv[1]);
                        } else {
                            log.error("Invalid command syntax");
                        }
                        break;
                    
                    case "exit":
                        process.exit(0);
                        break;
                }
            }
        });
    } else {
        log.fatal("TAUMAIC is already started");
    }
}



function client() {
    init();
    let data;

    // Load client api key
    try {
        let strdata = readFileSync("token.json", {encoding: "utf8"});
        data = JSON.parse(strdata);
    } catch(err) {
        log.fatal("Error during reading and parsing of token.json file: " + err.message);
    }

    if(!("api" in data)){
        log.fatal("File token.json must have key \"api\" with a valid api key");
        return;
    }

    const api = new APIClient(log, data["api"]);

    log.onInput((msg) => {
        let argv = msg.split(' ');
        if(argv.length > 0){
            let cmd = argv[0].toLowerCase();
            
            switch(cmd) {
                case "connect":
                    if(argv.length === 3){
                        api.connect(argv[1], Number.parseInt(argv[2])).then(() => {
                            log.success("Successfully connected");
                        }, (err) => {
                            log.error("Error: " + err.message);
                        });
                    } else {
                        log.error("Invalid command syntax");
                    }
                    break;

                case "update":
                    if(argv.length === 1){
                        printResponse( api.update() );
                    } else {
                        log.error("Invalid command syntax");
                    }
                    break;

                case "restart":
                    if(argv.length === 1){
                        printResponse( api.restart() );
                    } else {
                        log.error("Invalid command syntax");
                    }
                    break;

                case "status":
                    if(argv.length === 1){
                        printResponse( api.status() );
                    } else {
                        log.error("Invalid command syntax");
                    }
                    break;

                case "newkey":
                    if(argv.length === 2){
                        printResponse( api.newkey(argv[1]) );
                    } else {
                        log.error("Invalid command syntax");
                    }
                    break;
                    
                case "exit":
                    process.exit(0);
                    break;
                
                default:
                    log.error("Unknow command");
            }
        }
    });
}



function init() {
    log.showDebug(DEBUG);
    log.setInEnable(true);
    log.setInPrefix("> ");
    log.start();

    process.on("SIGINT", () => {
        process.exit();
    });

    process.on("SIGTERM", () => {
        process.exit();
    });

    process.on("exit", () => {
        if(serverapi !== null) {
            serverapi.saveKeyStore("apikeys.json");
        }
        lock.unlock();
        log.closeLogFile();
    });
}



function printResponse(pres : Promise<APIClientResponse>) {
    pres.then((res) => {
        if(res.haserror){
            log.error("Failled");
            if(res.errors.length > 0){
                log.error("Errors:");
                for(let i = 0; i < res.errors.length; i++){
                    log.error("  " + res.errors[i]);
                }
            }
        } else {
            log.success("Success");
            if(res.data !== null){
                log.success("Response:");
                log.success(JSON.stringify(res.data, null, 2));
            }
        }
    }, (err) => {
        log.error("Failled");
        log.error("Error: " + err.message);
    });
}



main();