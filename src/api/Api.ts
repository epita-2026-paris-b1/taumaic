"use strict";


/*
Security model:
    Public token: identifier.hash
        identifier: An UUID for each client
        hash: The hashed challenge + key + data
    
    | Server | <-- Ask connect with their UUID -- | Client |
    |        | ------ Send plain challenge -----> |        |
    |        | <--- Reply with challenge hash --- |        |
    |        |                                    |        |
    |        |                                    |        |
*/


export const API_VERSION = "1";
export const API_BASE_ENDPOINT = "/api/v" + API_VERSION + "/";


export interface APIToken {
    code: string,
    uuid: string
}


export function strToToken(value : string) : APIToken {
    let parts = value.split('.');
    if(parts.length == 1) return {uuid: parts[0], code: null};
    if(parts.length == 2) return {uuid: parts[0], code: parts[1]};
    return {uuid: null, code: null};
}


export function tokenToStr(token : APIToken) : string {
    if(!token.uuid) return null;
    let value = token.uuid;
    if(token.code) {
        value += "." + token.code;
    }
    return value;
}


export function parseAuthorizationHeader(value: string) : {type: string, value: string} {
    let parts = value.split(' ');
    if(parts.length !== 2) return null;
    return {type: parts[0], value: parts[1]};
}


export function readFullBody(req) : Promise<string> {
    return new Promise((resolve, reject) => {
        var body = "";
        req.on("readable", () => {
            let data = req.read();
            if(typeof data === "string") body += data;
            else if(data instanceof Buffer) body += data.toString("utf8");
        });
        req.on("end", () => resolve(body));
        req.on("error", reject);
    });
}
