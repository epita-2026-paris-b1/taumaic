import * as http from 'http'; 
import {
    createHmac,
    createSecretKey,
    KeyObject
} from 'crypto';

import {
    API_BASE_ENDPOINT,
    readFullBody,
    strToToken,
    parseAuthorizationHeader
} from './Api';

import { Logger } from '../Log';



export enum APIClientStates {
    CONNECTING,
    CONNECTED,
    CLOSED
}

export interface APIClientRequest {
    method: string,
    path: string,
    body: string,
    resolve: (response : APIClientResponse) => void,
    reject: (error : Error) => void
}

export interface APIClientResponse {
    httpcode : number,
    httpmessage : string,
    haserror : boolean,
    errors : string[],
    data : object,
    rawdata : string
}



// Main server class

export class APIClient {
    host: string;
    port: number;
    agent: http.Agent;
    key: KeyObject;
    uuid: string;
    challenge: string;
    state: APIClientStates;
    isqueueprocessing: boolean;
    queue: APIClientRequest[];
    log: Logger;

    public constructor(log : Logger, strtoken : string) {
        this.log = log;

        this.agent = new http.Agent({
            keepAlive: true,
            maxSockets: 1
        });

        let token = strToToken(strtoken);
        if(token !== null && token.uuid !== null && token.code !== null){
            this.key = createSecretKey(Buffer.from(token.code, "base64"));
            this.uuid = token.uuid;
        } else {
            throw new Error("Invalid token passed as argument");
        }

        this.challenge = null;
        this.state = APIClientStates.CLOSED;
        this.isqueueprocessing = false;
        this.queue = [];
    }

    public async connect(host : string, port : number) : Promise<void> {
        this.log.debug("Connected to " + host + " on port " + port);
        this.host = host;
        this.port = port;
        this.state = APIClientStates.CONNECTING;
        let res = await this.request("GET", API_BASE_ENDPOINT + "auth", "", true);
        if(res.httpcode !== 401 && res.haserror){
            this.state = APIClientStates.CLOSED;
            throw new Error(res.errors.join(", "));
        } else {
            this.state = APIClientStates.CONNECTED;
        }
    }

    public request(method : "GET" | "POST" | "PUT", path : string, body : string = "", ignorestate : boolean = false) : Promise<APIClientResponse> {
        return new Promise((resolve, reject) => {
            if(this.state === APIClientStates.CONNECTED || ignorestate){
                this.queue.push({
                    method,
                    path,
                    body: body || "",
                    resolve,
                    reject
                });
                this.processQueue();
            } else {
                reject(new Error("not connected"));
            }
        });
    }

    private async processQueue() : Promise<void> {
        if(!this.isqueueprocessing){
            this.isqueueprocessing = true;
            while(this.queue.length > 0){
                await this.processRequest(this.queue.shift());
            }
            this.isqueueprocessing = false;
        }
    }

    private processRequest(reqinfo : APIClientRequest) : Promise<void> {
        var self = this;
        return new Promise((resolve, _) => {
            this.log.debug("Make request " + reqinfo.method + " on " + reqinfo.path);

            const req = http.request({
                agent: self.agent,
                host: self.host,
                method: reqinfo.method,
                path: reqinfo.path,
                port: self.port,
                headers: self.buildRequestHeaders(reqinfo.method, reqinfo.path, reqinfo.body)
            });
    
            req.on("error", (err) => {
                reqinfo.reject(err);
                resolve();
            });
    
            req.on("response", async (res) => {
                let data = null;
                let rawdata;
                let errors = [];
                let haserror = false;

                // Read response header
                if("authorization" in res.headers){
                    let auth = parseAuthorizationHeader(res.headers["authorization"]);
                    if(auth.type !== "Challenge"){
                        reqinfo.reject(new Error("invalid authorization header"));
                        resolve();
                        return;
                    }
                    self.challenge = auth.value;
                } else {
                    reqinfo.reject(new Error("no authorization header"));
                    resolve();
                    return;
                }

                // Read response body
                try {
                    rawdata = await readFullBody(res);
                } catch(err) {
                    reqinfo.reject(err);
                    resolve();
                    return;
                }

                // Parse JSON
                if(rawdata){
                    try {
                        data = JSON.parse(rawdata);
                    } catch(err) {
                        haserror = true;
                        errors.push("body json parse");
                    }
                }

                // Check http code
                if(res.statusCode !== 200) {
                    haserror = true;
                    errors.push("http code " + res.statusCode.toString());
                }

                reqinfo.resolve({
                    httpcode: res.statusCode,
                    httpmessage: res.statusMessage,
                    haserror, errors, rawdata, data
                });

                resolve();
            });

            if(reqinfo.body !== null && reqinfo.body !== "") {
                req.write(reqinfo.body);
            }
    
            req.end();
        });
    }

    private buildRequestHeaders(method : string, path : string, body : string) {
        let token = this.uuid;
        
        if(this.challenge !== null) {
            let data = "";
            data += method;
            //data += this.host + ":" + this.port.toString() + path;
            if(body !== null) data += body;

            let hmac = createHmac("sha256", this.key);
            hmac.update(this.challenge);
            hmac.update(data);

            token += "." + hmac.digest("base64");
        }

        this.log.debug("Use token " + token + " computed with challenge " + this.challenge + " for next request");

        return {
            "Accept": "application/json",
            "Authorization": "Digest " + token,
            "User-Agent": "TAUMAIC/0.0.1 API Client"
        }
    }


    // Commands functions

    public update(){
        return this.request("POST", API_BASE_ENDPOINT + "update");
    }

    public newkey(name : string){
        return this.request("POST", API_BASE_ENDPOINT + "newkey", "{\"name\": \""+name+"\"}");
    }

    public restart(){
        return this.request("POST", API_BASE_ENDPOINT + "restart");
    }

    public status(){
        return this.request("GET", API_BASE_ENDPOINT + "status");
    }
}
