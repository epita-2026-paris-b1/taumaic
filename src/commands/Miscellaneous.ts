import { TAUMAIC } from "../Bot";
import {
    CommandFormat,
    CommandOptionFormat,
    CommandOptionChoice
} from "../Commands";
import {
    ApplicationCommandOptionType
} from "discord.js";


export function init(bot : TAUMAIC) {
    // Add help command
    /*bot.cmdmanager.addCommand(
        new CommandFormat("help", "Get help about TAUMAIC commands", [
            new CommandOptionFormat("command", "STRING", "Command to get help from", false, [
                new CommandOptionChoice("help",  "STRING", "help"),
                new CommandOptionChoice("ping",  "STRING", "ping"),
                new CommandOptionChoice("rng",   "STRING", "rng"),
                new CommandOptionChoice("play",  "STRING", "play"),
                new CommandOptionChoice("add",   "STRING", "add"),
                new CommandOptionChoice("stop",  "STRING", "stop"),
                new CommandOptionChoice("leave", "STRING", "leave"),
                new CommandOptionChoice("mode",  "STRING", "mode"),
                new CommandOptionChoice("*",     "STRING", "*")
            ])
        ]), (cmd, opts, rep) => {
            let queried = opts.get("command", "*").toLowerCase();
            if(queried === "*"){
                rep.reply(bot.help);
            } else {
                if(queried in bot.helps){
                    rep.reply(bot.helps[queried]);
                } else {
                    rep.reply("Can't find help for \""+queried+"\"");
                }
            }
        }
    );*/


    // Add ping command
    bot.cmdmanager.addCommand(
        new CommandFormat("ping", "Respond with pong!", []), (cmd, opts, rep) => {
            rep.reply("pong!");
        }
    );


    // Add rng command
    bot.cmdmanager.addCommand(
        new CommandFormat("rng", "Generate a random number", [
                new CommandOptionFormat("maximum", ApplicationCommandOptionType.Integer, "The upper limit (include)", false)
        ]), (cmd, args, replier) => {
            let max = args.get("maximum", 6);
            let value = Math.floor( Math.random() * max ) + 1;
            replier.reply(value.toString());
        }
    );
}


let allhelp = "Help:";
let helps = new Map<string, string>();


export function genhelpcmd(bot : TAUMAIC) {
    let choices = [];
    bot.cmdmanager.commands.forEach((cmd,x,y) => {
        choices.push(
            new CommandOptionChoice(cmd.format.name,  "STRING", cmd.format.name)
        );
        helps[cmd.format.name] = cmd.format.description;
        allhelp += "\n   " + cmd.format.name + " : " + cmd.format.description;
    });

    bot.cmdmanager.addCommand(
        new CommandFormat("help", "Get help about TAUMAIC commands", [
            new CommandOptionFormat("command", ApplicationCommandOptionType.String, "Command to get help from", false, choices)
        ]), (cmd, opts, rep) => {
            let queried = opts.get("command", "*").toLowerCase();
            if(queried === "*"){
                rep.reply(allhelp);
            } else {
                if(queried in helps){
                    rep.reply(helps[queried]);
                } else {
                    rep.reply("Can't find help for \""+queried+"\"");
                }
            }
        }
    );
}