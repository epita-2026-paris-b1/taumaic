import { TAUMAIC } from "../Bot";
import {
    CommandFormat,
    CommandOptionFormat,
    CommandOptionChoice,
    CommandReplier,
    CommandOptions
} from "../Commands";

import {
    ApplicationCommandOptionType
} from "discord.js";

import { musicQuery } from "../music/Music";
import { MusicSubscription } from "../music/Subscription";



// Common errors messages
const MSG_USER_NOT_IN_VOICE_CHANNEL = "Vous n'ête pas connecter à un salon vocal.";
const MSG_BOT_NOT_IN_VOICE_CHANNEL = "Le bot n'est pas dans votre salon vocal.";



export function init(bot : TAUMAIC) {
    // Add play command
    // (play music)
    bot.cmdmanager.addCommand(
        new CommandFormat("play", "Play a new track", [
            new CommandOptionFormat("query", ApplicationCommandOptionType.String, "Audio query")
        ], true), (cmd, args, replier) => {
            play(bot, replier, args);
        }
    );

    // Add play command
    // (play music)
    bot.cmdmanager.addCommand(
        new CommandFormat("clear", "Clear playlist", [
            new CommandOptionFormat("query", ApplicationCommandOptionType.String, "Audio query")
        ], true), (cmd, args, replier) => {
            clear(bot, replier);
        }
    );

    // Add add command
    // (add new track to play list)
    bot.cmdmanager.addCommand(
        new CommandFormat("add", "Add a track to play list", [
            new CommandOptionFormat("query", ApplicationCommandOptionType.String, "Audio query")
        ], true), (cmd, args, replier) => {
            addtrack(bot, replier, args);
        }
    );

    // Add add command
    // (add new track to play list)
    bot.cmdmanager.addCommand(
        new CommandFormat("goto", "Goto to a specific track", [
            new CommandOptionFormat("index", ApplicationCommandOptionType.Integer, "Track index", true)
        ], true), (cmd, args, replier) => {
            gototrack(bot, replier, args);
        }
    );

    // Add stop command
    // (stop bot from playing music)
    bot.cmdmanager.addCommand(
        new CommandFormat("stop", "Stop playing music", []), (cmd, args, replier) => {
            stop(bot, replier);
        }
    );

    // Add stop command
    // (stop bot from playing music)
    bot.cmdmanager.addCommand(
        new CommandFormat("pause", "Pause music", []), (cmd, args, replier) => {
            pause(bot, replier);
        }
    );

    // Add stop command
    // (stop bot from playing music)
    bot.cmdmanager.addCommand(
        new CommandFormat("remove", "Remove curent track", [
            new CommandOptionFormat("index", ApplicationCommandOptionType.Integer, "Track index")
    ], false), (cmd, args, replier) => {
            remove(bot, replier, args);
        }
    );

    // Add stop command
    // (stop bot from playing music)
    bot.cmdmanager.addCommand(
        new CommandFormat("curent", "Show curent music", []), (cmd, args, replier) => {
            curent(bot, replier);
        }
    );

    // Add stop command
    // (stop bot from playing music)
    bot.cmdmanager.addCommand(
        new CommandFormat("skip", "Skip curent tracks music", []), (cmd, args, replier) => {
            skip(bot, replier);
        }
    );

    // Set play mode command
    // (set play mode)
    bot.cmdmanager.addCommand(
        new CommandFormat("mode", "Set play mode", [
                new CommandOptionFormat("mode", ApplicationCommandOptionType.String, "Loop mode", true, [
                    new CommandOptionChoice("loop", "STRING", "loop", "Loop over the playlist"),
                    new CommandOptionChoice("loopone", "STRING", "loopone", "Loop over the curent playback"),
                    new CommandOptionChoice("noloop", "STRING", "noloop", "Don't loop over anything")
                ])
        ]), (cmd, args, replier) => {
            setmode(bot, replier, args);
        }
    );

    // Add leave command
    // (leave bot from voic channel)
    bot.cmdmanager.addCommand(
        new CommandFormat("leave", "Make bot leave your voice channel", []), (cmd, args, replier) => {
            leave(bot, replier);
        }
    );

    // Add tracks command
    // (list tracks)
    bot.cmdmanager.addCommand(
        new CommandFormat("tracks", "List all track of play list", []), (cmd, args, replier) => {
            listtracks(bot, replier);
        }
    );

    // Add shuffle command
    // (shuffle tracks)
    bot.cmdmanager.addCommand(
        new CommandFormat("shuffle", "Shuffle play list", []), (cmd, args, replier) => {
            shuffletracks(bot, replier);
        }
    );
}


// Ask bot to stop playing music and leave the channel
function leave(bot : TAUMAIC, replier : CommandReplier) : void {
    let channel = replier.member.voice.channel;
    if(channel) {
        // Create new MusicSubscription if don't exist for the user voice channel
        if(bot.musicsubscriptions.has(channel.id)){
            bot.musicsubscriptions.get(channel.id).leave();
            bot.musicsubscriptions.delete(channel.id);
            replier.reply({content: "Le bot a quitter votre channel vocal", ephemeral: false});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


function clear(bot : TAUMAIC, replier : CommandReplier) : void {
    let channel = replier.member.voice.channel;
    if(channel) {
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            musicsub.stop();
            musicsub.clearTracks();
            replier.reply({content: "Le liste de lecture a été vidé", ephemeral: false});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


function skip(bot : TAUMAIC, replier : CommandReplier) : void {
    let channel = replier.member.voice.channel;
    if(channel) {
        // Create new MusicSubscription if don't exist for the user voice channel
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            musicsub.skip();
            replier.reply({content: "La musique en cours a été passé", ephemeral: false});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


// Ask bot to stop playing music
function stop(bot : TAUMAIC, replier : CommandReplier) : void {
    let channel = replier.member.voice.channel;
    if(channel){
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            musicsub.stop();
            replier.reply({content: "Le bot a arrêté de jouer la musique", ephemeral: false});
        } else {
            // Bot not connected to user voice channel
            replier.reply({content: MSG_BOT_NOT_IN_VOICE_CHANNEL, ephemeral: true});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


function pause(bot : TAUMAIC, replier : CommandReplier) : void {
    let channel = replier.member.voice.channel;
    if(channel){
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            musicsub.pause();
            replier.reply({content: "La lecture de la musique à été mise en pause", ephemeral: false});
        } else {
            // Bot not connected to user voice channel
            replier.reply({content: MSG_BOT_NOT_IN_VOICE_CHANNEL, ephemeral: true});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


// Change bot play mode (loop all, loop one, no loop)
function setmode(bot : TAUMAIC, replier : CommandReplier, args : CommandOptions) : void {
    let channel = replier.member.voice.channel;
    if(channel){
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            let mode = args.get("mode");
            if(mode === "loop" || mode === "loopone" || mode === "noloop"){
                musicsub.setMode(mode);
            } else {
                // Unoknow mode
                replier.reply({content: "Mode inconue, veillez entrez l'une des valeurs suivante: loop, loopone ou noloop", ephemeral: true});
            }
        } else {
            // Bot not connected to user voice channel
            replier.reply({content: MSG_BOT_NOT_IN_VOICE_CHANNEL, ephemeral: true});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


// Add track(s) to play list
function addtrack(bot : TAUMAIC, replier : CommandReplier, args : CommandOptions) : void {
    let channel = replier.member.voice.channel;
    if(channel) {
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            replier.reply({content: "Traitement de la requete en cours ...", ephemeral: false});
            musicQuery(args.get("query")).then((response) => {
                for(let track of response.tracks)
                    musicsub.addTrack(track);
                let msg = "Piste audio ajouter à la liste de lecture (" +
                    response.tracks.length + " ajout" + (response.tracks.length > 1 ? 's' : '') + ")";
                replier.editReply({content: msg, ephemeral: false});
            }).catch((err) => {
                replier.editReply({content: err.toString(), ephemeral: false});
            });
        } else {
            // Bot not connected to user voice channel
            replier.reply({content: MSG_BOT_NOT_IN_VOICE_CHANNEL, ephemeral: true});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


// Remove track from play list
function remove(bot : TAUMAIC, replier : CommandReplier, args : CommandOptions) : void {
    let channel = replier.member.voice.channel;
    if(channel){
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            let index = args.get("index", musicsub.getCurent());
            musicsub.removeTrack(index);
        } else {
            // Bot not connected to user voice channel
            replier.reply({content: MSG_BOT_NOT_IN_VOICE_CHANNEL, ephemeral: true});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


function gototrack(bot : TAUMAIC, replier : CommandReplier, args : CommandOptions) : void {
    let channel = replier.member.voice.channel;
    if(channel){
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            let index = args.get("index");
            musicsub.goto(index);
        } else {
            // Bot not connected to user voice channel
            replier.reply({content: MSG_BOT_NOT_IN_VOICE_CHANNEL, ephemeral: true});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


// Add track to play list and jump to added track
function play(bot : TAUMAIC, replier : CommandReplier, args : CommandOptions) : void {
    let channel = replier.member.voice.channel;
    if(channel){
        // Create new MusicSubscription if don't exist for the user voice channel
        if(!bot.musicsubscriptions.has(channel.id)){
            bot.musicsubscriptions.set(channel.id, new MusicSubscription(channel, null));
        }

        let query = args.get("query", "");
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(query){
            replier.reply({content: "Traitement de la requete en cours ...", ephemeral: false});
            musicQuery(query).then((response) => {
                if(response.tracks.length > 0) {
                    for(let track of response.tracks)
                        musicsub.addTrack(track);
                    musicsub.goto(-1);
                    musicsub.play();
                    replier.editReply({content: "Lecture et ajout de la piste audio à la liste de lecture.", ephemeral: false});
                } else {
                    replier.editReply({content: "Aucun titres n'a peus être ajouté.", ephemeral: false});
                }
            }).catch((err) => {
                replier.editReply({content: err.toString(), ephemeral: false});
            });
        } else {
            musicsub.play();
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


// List tracks
function listtracks(bot : TAUMAIC, replier : CommandReplier) : void {
    let channel = replier.member.voice.channel;
    if(channel) {
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            let tracks = musicsub.getTracks();
            let msg = "Liste de lecture:";
            for(let i = 0; i < tracks.length; i++) {
                if(i === musicsub.getCurent()){
                    msg += "\n > " + tracks[i].title;
                } else {
                    msg += "\n   " + tracks[i].title;
                }
            }
            replier.reply({content: msg, ephemeral: false});
        } else {
            // Bot not connected to user voice channel
            replier.reply({content: MSG_BOT_NOT_IN_VOICE_CHANNEL, ephemeral: true});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


function curent(bot : TAUMAIC, replier : CommandReplier) : void {
    let channel = replier.member.voice.channel;
    if(channel){
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            if(musicsub.getCurent() === -1){
                replier.reply({content: "Aucune musique en cours de lecture.", ephemeral: false});
            } else {
                replier.reply({content: "Musique actuelle: " + musicsub.getTracks()[musicsub.getCurent()].title, ephemeral: false});
            }
        } else {
            // Bot not connected to user voice channel
            replier.reply({content: MSG_BOT_NOT_IN_VOICE_CHANNEL, ephemeral: true});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}


// Shuffle tracks
function shuffletracks(bot : TAUMAIC, replier : CommandReplier) : void {
    let channel = replier.member.voice.channel;
    if(channel){
        let musicsub = bot.musicsubscriptions.get(channel.id);
        if(musicsub){
            musicsub.shuffle(0);
            replier.reply({content: "Liste de lecture mélanger.", ephemeral: false});
        } else {
            // Bot not connected to user voice channel
            replier.reply({content: MSG_BOT_NOT_IN_VOICE_CHANNEL, ephemeral: true});
        }
    } else {
        // User not in voice channel
        replier.reply({content: MSG_USER_NOT_IN_VOICE_CHANNEL, ephemeral: true});
    }
}
