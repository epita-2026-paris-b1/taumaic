from youtube_dl import YoutubeDL
from time import time
from threading import Thread, Event, Lock
from io import StringIO
from queue import Queue
import json



# Constants
INSTANCE_TIMEOUT = 30_000 # In milliseconds
INSTANCE_MAX_COUNT = 8 # Maximum number of instances
QUERY_QUEUE_MAX_SIZE = 256 # Maximum number of waiting query



# Get time in milliseconds
def getTime():
    return int(time() * 1_000)


class VirtualLogger(object):
    def __init__(self):
        self.stderr = StringIO()
        self.stdout = StringIO()

    def debug(self, msg : str) -> None:
        self.stdout.write(msg)
        self.stdout.write('\n')

    def info(self, msg : str) -> None:
        self.stdout.write(msg)
        self.stdout.write('\n')

    def error(self, msg : str) -> None:
        self.stderr.write(msg)
        self.stderr.write('\n')

    def warning(self, msg : str) -> None:
        self.stderr.write(msg)
        self.stderr.write('\n')
    
    def clear(self):
        self.stderr.close()
        self.stdout.close()
        self.stderr = StringIO()
        self.stdout = StringIO()


instances_lock = Lock()

class YtbDlInstance:
    def __init__(self) -> None:
        self.instance = None
        self.logger = VirtualLogger()
        self.lastuse = getTime()
        self.thread = None
        self.query = None
        self.userdata = None
        self.callback = None
        self.bussy = False
        self.event = Event()
        self.stop = False
    
    def open(self) -> None:
        self.instance = YoutubeDL({
            "call_home": False,
            "extract_flat": "in_playlist",
            "logger": self.logger,
            "format": "bestaudio[acodec=opus]/bestaudio"
        })

    def _threadFunc(self) -> None:
        while True:
            self.event.wait(None)
            self.event.clear()
            if self.stop: return
            self.bussy = True
            res = {}
            try:
                res['content'] = self.instance.extract_info(self.query, download = False)
            except Exception as e:
                res = {"error": "youtubedl_error", "error_msg": str(e)}
            self.lastuse = getTime()
            self.bussy = False
            instances_lock.acquire()
            self.callback(res, self, self.userdata)
            instances_lock.release()

    def _startThread(self) -> None:
        self._stopThread()
        self.event.clear()
        self.stop = False
        self.thread = Thread(target = self._threadFunc)
        self.thread.start()

    def _stopThread(self) -> None:
        if self.thread and self.thread.is_alive():
            self.stop = True
            self.event.set()
            self.thread.join()
            self.thread = None

    def setCallback(self, callback) -> None:
        self.callback = callback

    def resolve(self, query : str, userdata = None) -> bool:
        if self.bussy: return False
        if not self.thread or not self.thread.is_alive():
            self._startThread()
        self.logger.clear()
        self.query = query
        self.userdata = userdata
        self.event.set()
        return True

    def getLogger(self):
        return self.logger

    def close(self) -> None:
        self._stopThread()
        self.logger.clear()
        del self.instance

    def getLastUse(self) -> int:
        return self.lastuse

    def isFree(self) -> bool:
        return not self.bussy


def answerToCommand(id : int, data : any) -> None:
    if data is None:
        print("{\"id\":%i}" % id, end = '\n', flush = True)
    else:
        data["id"] = id
        print(json.dumps(data, indent = None), end = '\n', flush = True)


def handleQueryResponse(data, instance, id) -> None:
    answerToCommand(id, data)


instances = []


def openInstance() -> YtbDlInstance:
    if len(instances) >= INSTANCE_MAX_COUNT:
        return None
    instance = YtbDlInstance()
    instance.setCallback(handleQueryResponse)
    instance.open()
    instances.append(instance)
    return instance

def getFreeInstance() -> YtbDlInstance:
    for instance in instances:
        if instance.isFree():
            return instance
    # No free instance available, so create a new one
    return openInstance()

def checkInstances() -> None:
    curent = getTime()
    for instance in instances:
        if curent - instance.getLastUse() > INSTANCE_TIMEOUT:
            closeInstance(instance)

def closeInstance(instance) -> None:
    instance.close()
    instances.remove(instance)


def parseCommandLine(cmdline):
    args = []
    tmparg = ""
    escape = False
    backslash = False

    for c in cmdline:
        if backslash:
            tmparg += c
        else:
            if c == '\\':
                backslash = True
            elif c == '\"':
                escape = True
            elif c == ' ' and escape == False:
                if tmparg:
                    args.append(tmparg)
                    tmparg = ""
            else:
                tmparg += c
    
    if tmparg:
        args.append(tmparg)

    return args


def parseInput(instr : str):
    pos = instr.find(' ')
    if pos == -1:
        return False, 0, None
    id = 0
    try:
        id = int(instr[:pos])
    except ValueError:
        return False, 0, None
    return True, id, instr[pos + 1:]


queries_queue = Queue(QUERY_QUEUE_MAX_SIZE)


def processQueriesQueue() -> None:
    if queries_queue.qsize() > 0:
        instance = getFreeInstance()
        # If there is no free instance then do nothing and wait next call to this function
        if instance is None: return
        id, query = queries_queue.get()
        instance.resolve(query, id)


def queryCommand(id : int, args : list):
    if queries_queue.qsize() >= QUERY_QUEUE_MAX_SIZE:
        # Queue is full
        return {"error": "full_queries_queue", "error_msg": "The queries queue is full"}
    if len(args) != 2:
        # Invalid arguments
        return {"error": "invalid_arguments_count", "error_msg": "This command must have only 1 argument"}
    query = args[1]
    queries_queue.put((id, query))
    processQueriesQueue()
    return None


def exitCommand():
    # Close each instance before quiting
    for instance in instances:
        instance.close()


# Return boolean indicate if main loop should break
def receiveCommand(id : int, args : list) -> bool:
    if len(args) == 0: return False
    cmd = args[0].lower()
    
    if cmd == "query":
        rep = queryCommand(id, args)
        if rep is not None:
            answerToCommand(id, rep)

    elif cmd == "quit":
        exitCommand()
        answerToCommand(id, None)
        return True

    else:
        answerToCommand(id, {"error": "unknow_command", "error_msg": "Unknow command \"" + cmd + "\""})

    return False


# Return boolean indicate if main loop should break
def receiveInput(incmd) -> bool:
    success, id, cmdline = parseInput(incmd)
    if success:
        args = parseCommandLine(cmdline)
        if args is None: return
        return receiveCommand(id, args)
    return False


def main() -> None:
    while True:
        instr = input()
        if receiveInput(instr): break

main()
